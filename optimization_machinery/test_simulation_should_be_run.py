import os
import yaml
import numpy as np
from netsquid_netconf.netconf import Loader
from optimization_machinery.analytical_model_for_one_repeater_key_rates import analyze_bb84_performance
from perform_optimizations.qkd.fixed_config.target_1_Hz.src.berlin_bonn_runscript import \
    simulation_should_be_run, extract_overall_attenuation_and_length_per_link, \
    attenuations_and_lengths_of_all_links_of_single_repeater_segments, compute_mode_success_probability_per_link, \
    compute_overall_success_probability_per_link, compute_round_times


def _build_path_and_extract_data(config_file):
    directory = os.path.dirname(os.path.realpath(__file__)) + "/"
    attenuations, lengths = \
        extract_overall_attenuation_and_length_per_link(config_file=directory + config_file)

    return attenuations, lengths


def _build_path_and_read_sim_params(param_file):
    directory = os.path.dirname(os.path.realpath(__file__)) + "/"
    with open(directory + param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)

    return sim_params


def test_extract_overall_attenuation_and_length_per_link(config_file="config_for_test_simulation_should_be_run.yaml"):
    attenuations, lengths = _build_path_and_extract_data(config_file)

    assert attenuations == [2.0, 4.0]
    assert lengths == [10., 20.]


def test_attenuations_and_lengths_of_all_links_of_single_repeater_segments(
        config_file="config_for_test_simulation_should_be_run.yaml"):
    attenuations, lengths = _build_path_and_extract_data(config_file)
    attenuations, lengths = attenuations_and_lengths_of_all_links_of_single_repeater_segments(attenuations=attenuations,
                                                                                              lengths=lengths)
    assert attenuations == [2.0]
    assert lengths == [10.]


def test_simulation_should_be_run(config_file="config_for_test_simulation_should_be_run.yaml",
                                  param_file="params_for_test_simulation_should_be_run.yaml"):
    attenuations, lengths = _build_path_and_extract_data(config_file)
    attenuations, lengths = attenuations_and_lengths_of_all_links_of_single_repeater_segments(attenuations=attenuations,
                                                                                              lengths=lengths)
    sim_params = _build_path_and_read_sim_params(param_file)
    mode_success_probabilities = compute_mode_success_probability_per_link(attenuations=attenuations,
                                                                           efficiency=sim_params['efficiency'])
    overall_success_probabilities = compute_overall_success_probability_per_link(
        mode_success_probabilities=mode_success_probabilities,
        n_modes=sim_params['num_modes'])
    round_times = compute_round_times(lengths=lengths,
                                      speed_of_light=sim_params["speed_of_light"])

    rate_upper_bounds = []
    for success_probability, round_time in zip(overall_success_probabilities, round_times):
        skr_upper_bound_sequential, _, _ = analyze_bb84_performance(link_success_prob=success_probability,
                                                                    round_time=round_time,
                                                                    coherence_time=sim_params["coherence_time"],
                                                                    cutoff_time=sim_params["cutoff_time"],
                                                                    only_dephasing=True,
                                                                    measure_directly=True)
        rate_upper_bounds.append(2 * skr_upper_bound_sequential)
    assert np.isclose(0.05565034708808247, rate_upper_bounds[0])

    directory = os.path.dirname(os.path.realpath(__file__)) + "/"
    target_rate = 0.1
    assert not simulation_should_be_run(config_file=directory + config_file,
                                        param_file=directory + param_file,
                                        target_rate=target_rate)
    target_rate = 0.001
    assert simulation_should_be_run(config_file=directory + config_file,
                                    param_file=directory + param_file,
                                    target_rate=target_rate)
