PYTHON        = python3
PIP           = pip3
PIP_FLAGS     = --extra-index-url=https://${NETSQUIDPYPI_USER}:${NETSQUIDPYPI_PWD}@pypi.netsquid.org

clean:
	@find . -name '*.pyc' -delete

lint:
	@$(PYTHON) -m flake8 .

requirements:
	@cat requirements.txt | xargs -n 1 $(PYTHON) -m pip install ${PIP_FLAGS}

tests:
	@$(PYTHON) -m pytest --cov=.

open-cov-report:
	@$(PYTHON) -m pytest --cov=. --cov-report html && xdg-open htmlcov/index.html

verify: clean python-deps lint tests_full examples

.PHONY: clean lint python-deps tests verify
