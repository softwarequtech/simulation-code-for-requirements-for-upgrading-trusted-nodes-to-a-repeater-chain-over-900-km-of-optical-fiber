import os
import csv
import fnmatch
from argparse import ArgumentParser
import matplotlib.pyplot as plt


def find_population_best_and_average(directory, required_cost=None):
    """Finds best and average of cost function in population per generation in a given directory containing optimization
    results.

    Parameters
    ----------
    directory : str
        Path to directory containing optimization results.

    Returns
    -------
    best_per_gen : list
        List of best cost function values per generation.
    av_per_gen : list
        List of average cost function values per generation.

    """
    n_gens = len(fnmatch.filter(os.listdir(directory), "param_set_*")) - 1
    best_per_gen = []
    av_per_gen = []
    min_gen = -1
    for i in range(n_gens):
        try:
            csv_file = open(directory + "/csv_output_" + str(i) + ".csv")
        except FileNotFoundError:
            try:
                csv_file = open(directory + "opt_step_" + str(i) + "/csv_output.csv")
            except FileNotFoundError:
                continue
        csv_reader = csv.reader(csv_file, delimiter=',')
        gen_vals = []
        for row in csv_reader:
            gen_vals.append(float(row[0]))
        csv_file.close()
        if len(gen_vals) == 0:
            continue
        best_per_gen.append(min(gen_vals))
        av_per_gen.append(sum(gen_vals) / len(gen_vals))
        if required_cost is not None and min_gen == -1:
            if best_per_gen[-1] < required_cost:
                min_gen = i
    return best_per_gen, av_per_gen, min_gen


def compute_variation(list_of_values, generation_range):
    """Computes percentual variation per generation w.r.t. to the average over the last `generation_range` generations.
    `None` is used for the first `generation_range` entries, as the quantity is ill-defined there.

    Parameters
    ----------
    list_of_values : list
        List of values for each variation should be computed.
    generation_range : int
        Number of generations over which variation should be computed.

    Returns
    -------
    variation_values : list
        List containing the percentual variation over the last `generation_range` generations for each generation.

    """
    variation_values = []
    for i in range(len(list_of_values)):
        if i < generation_range:
            variation_values.append(None)
            continue
        relevant_values = [list_of_values[j] for j in range(i - generation_range, i - 1)]
        average_last_gens = sum(relevant_values) / len(relevant_values)
        pcnt_variation = (list_of_values[i] - average_last_gens) / average_last_gens * 100
        variation_values.append(pcnt_variation)

    return variation_values


def plot_against_generation(list_of_values, label):
    """Plotting functionality. Barebones for now.
    """
    plt.scatter(range(len(list_of_values)), list_of_values)
    plt.show()


def check_convergence(list_of_variations, accepted_variation, min_gen):
    """Checks if convergence was attained by comparing the computed variations to the tolerance.

    Parameters
    ----------
    list_of_variations : list
        List containing computed variations per generation.
    accepted_variation : float
        Tolerance for convergence. Given in percentage.

    Returns
    -------
    int
        Generation in which convergence was attained. -1 if there was no convergence.

    """
    for i, entry in enumerate(list_of_variations):
        if i > min_gen:
            if type(entry) is float and abs(entry) < accepted_variation:
                return i
    return -1


def save_data(conv_gen, directory, output_file):
    """
    Saves directory and generation of convergence to specified output file.

    Parameters
    ----------
    conv_gen : int
        Generation in which convergence occurred. -1 if there was no convergence.
    directory : str
        Path to directory containing optimization results.
    output_file : str
        Name of file in which to write output.
    """
    directory = directory.split("/")[0]
    with open(output_file, 'a+') as f:
        f.write(str(directory) + "," + str(conv_gen))
        f.write("\n")


def determine_convergence(directory, average_or_best, generation_range, accepted_variation, required_cost=None):
    """Determines if optimization procedure whose results are stored in `directory` converged according to user-defined
    criterion.

    Parameters
    ----------
    directory : str
        Path to directory containing optimization results.
    average_or_best : str
        Whether 'average' cost function value or 'best' cost function value should be used to determine convergence.
    generation_range : int
        Number of generations over which variation should be computed.
    accepted_variation : float
        Tolerance for convergence. Given in percentage.

    """
    directory += "/optimization/"
    best_per_gen, av_per_gen, min_gen = find_population_best_and_average(directory=directory,
                                                                         required_cost=required_cost)
    if average_or_best == "average":
        av_per_gen_variation = compute_variation(list_of_values=av_per_gen, generation_range=20)
        conv_gen = check_convergence(list_of_variations=av_per_gen_variation,
                                     accepted_variation=accepted_variation,
                                     min_gen=min_gen)
    if average_or_best == "best":
        best_per_gen_variation = compute_variation(list_of_values=best_per_gen, generation_range=20)
        conv_gen = check_convergence(list_of_variations=best_per_gen_variation,
                                     accepted_variation=accepted_variation,
                                     min_gen=min_gen)
    else:
        raise ValueError("average_or_best should be 'average' or 'best', not {}.".format(average_or_best))
    return conv_gen


def determine_convergence_and_save_data(directory, average_or_best, generation_range, accepted_variation, output_file):
    """Determines if optimization procedure whose results are stored in `directory` converged according to user-defined
    criterion, and stores output in `output_file`.

    Parameters
    ----------
    directory : str
        Path to directory containing optimization results.
    average_or_best : str
        Whether 'average' cost function value or 'best' cost function value should be used to determine convergence.
    generation_range : int
        Number of generations over which variation should be computed.
    accepted_variation : float
        Tolerance for convergence. Given in percentage.
    output_file : str
        Name of file in which to write output.

    """
    conv_gen = determine_convergence(directory, average_or_best, generation_range, accepted_variation)
    save_data(conv_gen, directory, output_file)

    if conv_gen < 0:
        print("Optimization did not converge according to specified criterion.")
    else:
        print("Optimization converged in generation {}.".format(conv_gen))


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--directory', type=str, help="Directory holding optimization results.")
    parser.add_argument('--average_or_best', type=str,
                        help="""Whether 'average' cost function value or 'best' cost function value
                                should be used to determine convergence.""")
    parser.add_argument('--generation_range', type=int, help="Number of generations over which to measure variation.")
    parser.add_argument('--accepted_variation', type=float, help="Tolerance for convergence. Given in percentage.")
    parser.add_argument('--output_file', type=str, help="Name of file in which to write output.")

    args, unknown = parser.parse_known_args()
    determine_convergence_and_save_data(directory=args.directory, average_or_best=args.average_or_best,
                                        generation_range=args.generation_range,
                                        accepted_variation=args.accepted_variation, output_file=args.output_file)
