---------------------------------------
## Finding minimal hardware requirements

In order to find minimal hardware requirements, the genetic-algorithm optimization methodology introduced in the paper must be employed.
The files required for doing so can be found in the `src` folders in the subdirectories.
Different subdirectories correspond to the different setups considered in the paper.
In particular:

 - `qkd/free_config/n_repeaters` - Minimal hardware requirements for 10 Hz QKD with n repeaters.
 - `qkd/free_config/free_repeaters` - Minimal hardware requirements for 10 Hz QKD with any number of repeaters (up to 7, the maximum allowed by the fiber path).
 - `qkd/free_config/some_repeaters` - Minimal hardware requirements for 10 Hz QKD with max 5 repeaters.
 - `qkd/fixed_config/target_1_Hz` - Minimal hardware requirements for 1 Hz QKD in the best configuration found (see paper for details).
 - `qkd/fixed_config/target_100_Hz` - Minimal hardware requirements for 100 Hz QKD in the best configuration found (see paper for details).
 - `bqc` - Minimal hardware requirements for 10 Hz BQC in the best configuration found (see paper for details).

The files included are:

 - `input_file.ini` - Defines choice of hyperparameters for genetic algorithm (including number of generations, mutation and crossover probabilities, population size and number of parents; for more details, see arXiv:2010.16373), parameters to be optimized, which scripts should be used to run the simulation and to process data and which configuration and parameter files should be passed as input to the simulation. The fields `sstoposdir` and `venvdir` define the directories in which `smart-stopos` is installed and where the virtual environment to use is defined. These should be updated to match the user's definitions. See the README in the `smart-stopos` [repository](https://gitlab.com/aritoka/smart-stopos) for details.
 - `abstract_baseline_params.yaml` - `YAML` file containing the baseline parameters from which to optimize. For further details on the meaning of 'baseline parameters', see the acommpanying paper.
 - `dummy.yaml` - Empty `YAML` file that the optimization method will populate with different configurations.
 - `processing_function.py` - Script used for processing of simulation data. From the simulation's output, which consists of dataframes containing density matrix and time required for entanglement generation, computes the corresponding cost function, as defined in the paper.
 - `run_local.sh`
 - `run_local_bg.sh` - This file and the above are used to execute the optimization procedure. They can both be used either locally, in a regular computer, or on a cluster that makes use of `slurm`.
 - `run_multiple_optimizations.sh` - Can be used on a cluster that makes use of `slurm` to submit multiple optimization runs simultaneously.
 - `berlin_bonn_runscript.py` - Layer of translation around the `unified_simulation_script.py` that is used to run the simulations. This is used to convert parameters from probabilities of no-error to their actual values and to generate the necessary configuration files.

For example, in order to execute an optimization procedure on your machine, you might execute the command:

`./run_local.sh`.

Alternatively,

`./run_local_bg.sh`,

has the same effect, but results in the optimization running on the background. If you wish to execute the procedure on a cluster, you should instead run:

`sbatch ./run_local_bg.sh`.

If you wish to perform `n` optimization runs of the same setup, you can run:

`./run_multiple_optimizations n`.

