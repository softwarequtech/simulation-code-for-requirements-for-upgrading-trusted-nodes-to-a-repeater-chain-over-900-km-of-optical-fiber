import numpy as np
import shutil
import yaml
import csv
from ruamel.yaml import YAML
from argparse import ArgumentParser
from netsquid_netconf.netconf import Loader
from demonstrate_ga_integration.qkd.color_center.src.processing_function import \
    compute_success_prob_from_num_modes, average_total_attenuation_from_file
from netsquid_qrepchain.simulations.unified_simulation_script_qkd import run_unified_simulation_qkd
from netsquid_qrepchain.simulations.unified_simulation_script_state import run_unified_simulation_state
from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, process_data_bb84, \
    process_data_bqc

TO_PROB_NO_ERROR_FUNCTION = {"efficiency": lambda x: x,
                             "num_modes": lambda x, fiber_data_file="Berlin_Bonn_fiber_data.csv":
                             compute_success_prob_from_num_modes(x, fiber_data_file),
                             "coherence_time": lambda x: np.exp(-1 * (1 / x)**2),
                             "swap_quality": lambda x: x,
                             "fidelity": lambda x: x,
                             }

TO_PARAMETER_VALUE = {"efficiency": lambda x: x,
                      "num_modes": lambda x, fiber_data_file="Berlin_Bonn_fiber_data.csv":
                      compute_num_modes_from_survival_prob(x, fiber_data_file),
                      "coherence_time": lambda x: np.sqrt(-1 / np.log(x)),
                      "swap_quality": lambda x: x,
                      "fidelity": lambda x: x,
                      }

baseline_parameters = {"efficiency": 0.255,
                       "num_modes": 1,
                       "coherence_time": 1.e+9,
                       "swap_quality": 0.8302189115834032,
                       "fidelity": 0.83}


def compute_num_modes_from_survival_prob(success_prob, fiber_data_file="Berlin_Bonn_fiber_data.csv"):
    """Given photon survival probability, computes the number of modes.
    """
    average_total_attenuation = average_total_attenuation_from_file(fiber_data_file)
    baseline_survival_probability = 10 ** (- average_total_attenuation / 10)

    return int(np.log(1 - success_prob) / (np.log(1 - baseline_survival_probability)))


def compute_parameter_cost(param_name, param_value):
    """Computes cost of single parameter.
    """
    if param_name == "coherence_time":
        return (param_value / baseline_parameters[param_name]) ** 2
    else:
        return 1 / (np.log(TO_PROB_NO_ERROR_FUNCTION[param_name](param_value)) /
                    np.log(TO_PROB_NO_ERROR_FUNCTION[param_name](baseline_parameters[param_name])))


def compute_parameter_dict_cost(parameter_dict):
    """Computes overall hardware cost associated to parameters defined in `parameter_dict`.
    """
    parameter_cost = 0
    for name, value in parameter_dict.items():
        parameter_cost += compute_parameter_cost(name, value)
    return parameter_cost


def reduce_cost(param_name, param_value, reduction):
    """Computes the value `param_name` should take given that its previous value was `param_value` and that its cost
    should be reduced by `reduction`.
    """
    parameter_cost = compute_parameter_cost(param_name, param_value)
    if param_name == "coherence_time":
        new_val = np.sqrt(baseline_parameters[param_name] ** 2 * (parameter_cost - reduction))
    else:
        new_prob_no_error_function = \
            np.exp(
                np.log(
                    TO_PROB_NO_ERROR_FUNCTION[param_name](
                        baseline_parameters[param_name])) / (parameter_cost - reduction))
        new_val = TO_PARAMETER_VALUE[param_name](new_prob_no_error_function)
    return new_val


def update_param_file(parameter, value, param_file):
    """Updates the `value` of `parameter` in `param_file`.
    """
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream=stream, Loader=Loader)
    sim_params[parameter] = float(value)
    with open(param_file, "w") as stream:
        yaml.dump(sim_params, stream)


def params_fulfill_target(config_file, param_file, target_rate, n_runs, qkd):
    """Performs simulation and evaluates whether parameters defined in `param_file` achieve `target_rate` on the
    configuration defined in `config_file`.
    """
    if qkd:
        processed_data = run_and_process_qkd(config_file=config_file, param_file=param_file, n_runs=n_runs)
        return True if float(processed_data["sk_rate"][0]) >= target_rate else False
    else:
        processed_data = run_and_process_bqc(config_file=config_file, param_file=param_file, n_runs=n_runs)
        return True if float(processed_data["bqc_success_rate"][0]) >= target_rate else False


def write_to_file(dictionary, output_file_name):
    """Writes dictionary to csv file, with the keys on the first row and the respective values on the second.

    Parameters
    ----------
    dictionary : dict
        Dictionary to be written.
    output_file_name : str
        File to write.
    """

    with open(output_file_name + '.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(dictionary.keys())
        csv_writer.writerow(dictionary.values())


def create_parameter_file(param_dict, output_file_name, path_to_old_param_file):
    """Creates new parameter file from previous parameter file and a parameter dictionary. Does so by taking the old
    parameter file, reading into a dictionary, overwriting the values of the keys also present in `param_dict` with
    their values in `param_dict` and dumping the result into `output_file_name`.

    Parameters
    ----------
    param_dict : dict
        Dictionary where keys are parameter names and values are parameter values.
    output_file_name : str
        Name of file where to dump new parameter dictionary.
    path_to_old_param_file : str
        Path to old parameter file.

    """
    new_param_file_name = output_file_name + '_params.yaml'
    with open(path_to_old_param_file, "r") as stream:
        old_params = yaml.load(stream, Loader=Loader)
    for k, v in param_dict.items():
        old_params[k] = float(v)
    with open(new_param_file_name, "w") as stream:
        yaml.dump(old_params, stream)


def make_parameter_dict(param_file, parameters):
    """Creates dictionary in which the keys are the names of the parameters for which the cost-reduction procedure will
    be performed and the values are their respective values in the input parameter file.
    """

    param_dict = {}
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream=stream, Loader=Loader)
    for param in parameters:
        param_dict[param] = sim_params[param]
    return param_dict


def make_new_config_file(config_file, param_file):
    """Creates new configuration file which is identical to the one fed into the script with the exception that it
    includes the new locally-optimized parameter file.
    """
    new_config_file_name = "locally_optimized_solution_config.yaml"
    shutil.copy(config_file, new_config_file_name)
    params_identifier = 'params'

    yaml = YAML()
    with open(new_config_file_name, 'r') as stream:
        code = yaml.load(stream)
    code[params_identifier]["INCLUDE"].value = param_file
    with open(new_config_file_name, "w") as stream:
        yaml.dump(code, stream)

    return new_config_file_name


def run_and_process_qkd(config_file, param_file, n_runs):
    """Perform QKD simulation and process data accordingly.
    """
    repchain_df_holder, _ = run_unified_simulation_qkd(configfile=config_file,
                                                       paramfile=param_file,
                                                       n_runs=n_runs,
                                                       suppress_output=True)

    return process_repchain_dataframe_holder(repchain_dataframe_holder=repchain_df_holder,
                                             processing_functions=[process_data_bb84])


def run_and_process_bqc(config_file, param_file, n_runs):
    """Perform BQC simulation and process data accordingly.
    """
    repchain_df_holder, _ = run_unified_simulation_state(configfile=config_file,
                                                         paramfile=param_file,
                                                         n_runs=n_runs,
                                                         suppress_output=True)

    return process_repchain_dataframe_holder(repchain_dataframe_holder=repchain_df_holder,
                                             processing_functions=[process_data_bqc])


def evaluate_performance_locally_optimized_solution(config_file, param_file, qkd, n_runs=100):
    """Runs simulation for the post-local-optimization parameter set, processes data and saves it.
    """
    new_config_file = make_new_config_file(config_file, param_file)
    if qkd:
        processed_data = run_and_process_qkd(config_file=new_config_file, param_file=param_file, n_runs=n_runs)
    else:
        processed_data = run_and_process_bqc(config_file=new_config_file, param_file=param_file, n_runs=n_runs)
    processed_data.to_csv(new_config_file.split(".yaml")[0] + "_output.csv", index=False)


def perform_local_optimization(config_file, param_file, target_rate, parameters, reduction=1, n_runs=100, qkd=True):
    """This function can be used to perform a local optimization over a given configuration and set of parameters meant
    to achieve a certain performance target.

    We start with a set of parameters defined in `param_file`. It is assumed that these parameters are good enough to
    achieve `target_rate` on the configuration defined by `config_file` (`target_rate` refers to secret-key rate if qkd
    is True and to rate of BQC test protocol success otherwise). We iterate over the hardware parameters, decreasing
    each of them by a value corresponding to decreasing their cost by `reduction`. We then run the simulation `n_runs`
    times. If `target_rate` is still achieved, we repeat the process. If not, we go to another parameter and execute the
    same procedure. The process concludes when this has been done for all parameters.

    Finally, the performance of the resulting reduced-cost hardware parameter set is evaluated and stored under
    `locally_optimized_solution_config_output.csv`.

    Parameters
    ----------
    config_file : str
        Name of file with configuration of network to be simulated.
    param_file : str
        Name of parameter file.
    target_rate : float, Hz
        Performance target. Can either be SKR or BQC protocol success rate.
    parameters : list
        List of names of parameters whose associated cost should be reduced.
    reduction : int
        Stepsize of cost reduction.
    n_runs : int
        Number of times simulation should be run per data point.
    qkd : bool
        If True, QKD simulations are performed. Otherwise, BQC.

    """
    shutil.copy(param_file, param_file.split('.yaml')[0] + "_copy.yaml")
    parameter_dict = make_parameter_dict(param_file, parameters)
    for parameter in parameters:
        print("Varying {}.".format(parameter))
        while True:
            new_param_value = reduce_cost(param_name=parameter,
                                          param_value=parameter_dict[parameter],
                                          reduction=reduction)
            print("New {} value: {}.".format(parameter, new_param_value))
            update_param_file(parameter=parameter, value=new_param_value, param_file=param_file)
            if not params_fulfill_target(config_file, param_file, target_rate, n_runs, qkd):
                break
            else:
                parameter_dict[parameter] = new_param_value
    parameter_dict["cost"] = compute_parameter_dict_cost(parameter_dict)
    write_to_file(parameter_dict, "locally_optimized_solution")
    create_parameter_file(parameter_dict, "locally_optimized_solution", param_file)
    shutil.copy(param_file.split('.yaml')[0] + "_copy.yaml", param_file)
    evaluate_performance_locally_optimized_solution(config_file=config_file,
                                                    param_file='locally_optimized_solution_params.yaml',
                                                    qkd=qkd,
                                                    n_runs=n_runs)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('config_file', type=str, help="Name of the config file.")
    parser.add_argument('param_file', type=str, help="Name of the parameter file.")
    parser.add_argument('-n', '--n_runs', required=False, type=int, default=100,
                        help="Number of runs per configuration. Defaults to 100.")
    parser.add_argument('--reduction', required=False, type=int, default=1,
                        help="Cost value by which parameters should be made worse on each step. Defaults to 1.")
    parser.add_argument('--target_rate', type=float)
    parser.add_argument('--qkd', action='store_true')
    parser.add_argument('--bqc', dest='qkd', action='store_false')
    parser.set_defaults(qkd=True)

    args, unknown = parser.parse_known_args()
    parameters = ["num_modes", "efficiency", "coherence_time", "swap_quality", "fidelity"]
    perform_local_optimization(config_file=args.config_file,
                               param_file=args.param_file,
                               n_runs=args.n_runs,
                               target_rate=args.target_rate,
                               parameters=parameters,
                               reduction=args.reduction,
                               qkd=args.qkd)
