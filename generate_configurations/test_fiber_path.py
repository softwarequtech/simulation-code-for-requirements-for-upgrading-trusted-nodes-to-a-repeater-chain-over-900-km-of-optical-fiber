from generate_configurations.fiber_path import FiberPath
import unittest
import numpy as np


mock_lengths = [10, 20, 30]
mock_attenuation_coefficients = [0.1, 0.2, 0.3]
mock_labels = ["node_1", "node_2", "node_3", "node_4"]
# node_1 -10- node_2 -20- node_3 -30- node_4
# note: the node_index of node_2 is 1, and of node_3 is 2 (not defined for the others)


class TestFiberPath(unittest.TestCase):
    def setUp(self):
        self.fiber_path = FiberPath(lengths=mock_lengths, attenuation_coefficients=mock_attenuation_coefficients,
                                    labels=mock_labels)

    def test_check_validity(self):
        with self.assertRaises(ValueError):
            FiberPath(lengths=mock_lengths, attenuation_coefficients=mock_attenuation_coefficients[:-1],
                      labels=mock_labels)
        with self.assertRaises(ValueError):
            FiberPath(lengths=mock_lengths, attenuation_coefficients=mock_attenuation_coefficients,
                      labels=mock_labels[:-1])
        with self.assertRaises(ValueError):
            FiberPath(lengths=mock_lengths + [40], attenuation_coefficients=mock_attenuation_coefficients,
                      labels=mock_labels)

    def test_properties(self):
        assert self.fiber_path.lengths == mock_lengths
        assert self.fiber_path.attenuation_coefficients == mock_attenuation_coefficients
        assert self.fiber_path.labels == mock_labels
        assert self.fiber_path.num_nodes == 4
        assert self.fiber_path.num_segments == 3
        assert self.fiber_path.total_length == 60

    def test_split_at_node(self):
        with self.assertRaises(ValueError):
            self.fiber_path.split_at_node(0)
        with self.assertRaises(ValueError):
            self.fiber_path.split_at_node(4)
        fiber_path_1, fiber_path_2 = self.fiber_path.split_at_node(1)
        # split at S: N - S - N - N
        assert fiber_path_1.labels[-1] == fiber_path_2.labels[0]
        assert fiber_path_1.num_nodes == 2
        assert fiber_path_2.num_nodes == 3
        assert fiber_path_1.num_segments == 1
        assert fiber_path_2.num_segments == 2
        assert fiber_path_1.lengths == [10]
        assert fiber_path_2.lengths == [20, 30]
        assert fiber_path_1.attenuation_coefficients == [0.1]
        assert fiber_path_2.attenuation_coefficients == [0.2, 0.3]
        assert fiber_path_1.labels == ["node_1", "node_2"]
        assert fiber_path_2.labels == ["node_2", "node_3", "node_4"]
        fiber_path_1._check_validity()
        fiber_path_2._check_validity()

    def test_join(self):
        extra_lengths = [40, 50]
        extra_attenuation_coefficients = [0.4, 0.5]
        extra_labels = ["node_4", "node_5", "node_6"]
        with self.assertRaises(ValueError):
            # check there is an error when node labels don't match
            extra_labels_wrong = ["node_wrong", "node_5", "node_6"]
            extra_fiber_path_wrong = FiberPath(lengths=extra_lengths,
                                               attenuation_coefficients=extra_attenuation_coefficients,
                                               labels=extra_labels_wrong)
            self.fiber_path.join(extra_fiber_path_wrong)
        extra_fiber_path = FiberPath(lengths=extra_lengths, attenuation_coefficients=extra_attenuation_coefficients,
                                     labels=extra_labels)
        new_fiber_path = self.fiber_path.join(extra_fiber_path)
        assert new_fiber_path.num_nodes == 6
        assert new_fiber_path.num_segments == 5
        assert new_fiber_path.lengths == [10, 20, 30, 40, 50]
        assert new_fiber_path.attenuation_coefficients == [0.1, 0.2, 0.3, 0.4, 0.5]
        assert new_fiber_path.labels == ["node_1", "node_2", "node_3", "node_4", "node_5", "node_6"]
        new_fiber_path._check_validity()

    def test_remove_node(self):
        with self.assertRaises(ValueError):
            self.fiber_path.remove_node(0)
        with self.assertRaises(ValueError):
            self.fiber_path.remove_node(4)
        self.fiber_path = self.fiber_path.remove_node(1)
        assert self.fiber_path.num_nodes == 3
        assert self.fiber_path.num_segments == 2
        assert self.fiber_path.lengths == [30, 30]
        assert self.fiber_path.attenuation_coefficients == [5 / 30, 0.3]  # 5 dB total after combining
        assert self.fiber_path.labels == ["node_1", "node_3", "node_4"]
        self.fiber_path._check_validity()

    def test_asymmetry_parameter(self):
        fiber_path = FiberPath(lengths=[10] * 10, attenuation_coefficients=[0.1] * 10, labels=["some_label"] * 11)
        assert fiber_path.asymmetry_parameter == 0
        assert np.isclose(self.fiber_path.asymmetry_parameter, (10 / 30 + 10 / 50) / 2)
