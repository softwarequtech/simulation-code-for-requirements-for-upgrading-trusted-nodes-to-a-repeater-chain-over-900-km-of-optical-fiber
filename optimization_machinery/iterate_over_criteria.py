import os
import fnmatch
import numpy as np
from argparse import ArgumentParser
from determine_ga_convergence import determine_convergence, find_population_best_and_average


def find_valid_optimization_directories(directory, dirbasename, generation_range):
    """Checks that optimization directories under `directory` have data for at least `generation_range` generations.

    Parameters
    ----------
    directory : str
        Directory holding data for possibly multiple optimization runs
    dirbasename : str
        Common start of optimization directories' names.
    generation_range : int
        Number of generations over which variation should be computed.

    Returns
    -------
    list_valid_directories : list
        List of paths to valid directories.

    """
    list_trial_directories = fnmatch.filter(os.listdir(directory), dirbasename + "*")
    list_valid_directories = \
        [opt_dir for opt_dir in list_trial_directories if
            len(fnmatch.filter(os.listdir(directory + "/" + opt_dir + "/optimization/"), "csv_output*"))
            >= generation_range]
    return list_valid_directories


def remove_non_converging_directories(directory, list_optimization_runs_directories,
                                      max_tolerance, generation_range, required_cost):
    """Removes directories that do not converge for `max_tolerance` from `list_optimization_runs_directories`.

    Parameters
    ----------
    directory : str
        Directory holding data for possibly multiple optimization runs
    list_optimization_runs_directories : list
        List of paths to valid directories.
    max_tolerance : float
        Maximum accepted variation (tolerance).
    generation_range : int
        Number of generations over which variation should be computed.
    required_cost : float
        Directories that contain no solutions with cost below this value are not considered.

    """
    list_converging_directories = []
    for opt_dir in list_optimization_runs_directories:
        conv_gen = determine_convergence(directory=directory + "/" + opt_dir, average_or_best="best",
                                         generation_range=generation_range, accepted_variation=max_tolerance,
                                         required_cost=required_cost)
        if conv_gen != -1:
            list_converging_directories.append(opt_dir)
    return list_converging_directories


def remove_directories_with_no_solutions(directory, list_optimization_runs_directories, required_cost):
    """Removes directories that do not have any solution with cost under `required_cost`.

    Parameters
    ----------
    directory : str
        Directory holding data for possibly multiple optimization runs
    list_optimization_runs_directories : list
        List of paths to valid directories.
    required_cost : float
        Directories that contain no solutions with cost below this value are not considered.

    """

    list_directories_with_solutions = []
    for opt_dir in list_optimization_runs_directories:
        best_per_gen, _, _ = find_population_best_and_average(directory=directory + "/" + opt_dir + "/optimization/")
        if min(best_per_gen) < required_cost:
            list_directories_with_solutions.append(opt_dir)
    return list_directories_with_solutions


def find_criterion(directory, generation_range, start_tolerance, tolerance_step, required_cost=1.e+100):
    """Finds criterion such that all optimization runs in `directory` converge. Does so by sweeping over different
    values of accepted variation (tolerance), starting at `start_tolerance` and increasing by `tolerance_step`.

    Starts by ignoring directories that do not converge for any accepted variation smaller than 10% over the desired
    `generation_range`. Also does not consider directories with no solutions with cost under `required_cost`.

    Parameters
    ----------
    directory : str
        Directory holding data for possibly multiple optimization runs
    generation_range : int
        Number of generations over which variation should be computed.
    start_tolerance : float
        Start point for sweep of tolerance for convergence. Given in percentage.
    tolerance_step : float
        Step for tolerance for convergence. Given in percentage.
    required_cost : float
        Directories that contain no solutions with cost below this value are not considered.

    """
    list_optimization_runs_directories = find_valid_optimization_directories(directory, "DemonstrateDeutscheTelekom",
                                                                             generation_range)
    list_optimization_runs_directories = remove_non_converging_directories(directory,
                                                                           list_optimization_runs_directories,
                                                                           10,
                                                                           generation_range,
                                                                           required_cost)

    list_optimization_runs_directories = remove_directories_with_no_solutions(directory,
                                                                              list_optimization_runs_directories,
                                                                              required_cost)
    for tolerance in np.arange(start_tolerance, 10, tolerance_step):
        for opt_directory in list_optimization_runs_directories:
            conv_gen = determine_convergence(directory=directory + "/" + opt_directory, average_or_best="best",
                                             generation_range=generation_range, accepted_variation=tolerance,
                                             required_cost=required_cost)
            if conv_gen == -1:
                break
        else:
            break

    return tolerance


def store_criterion(tolerance, generation_range, directory, output_file):
    """Stores successful convergence criterion, consisting of a `tolerance` and a `generation_range`, as well as
    respective directory, in given `output_file`.

    Parameters
    ----------
    tolerance : float
        Tolerance for convergence. Given in percentage.
    generation_range : int
        Number of generations over which variation should be computed.
    directory : str
        Directory holding data for possibly multiple optimization runs
    output_file : str
        Name of file where to store successful convergence criterion.

    """
    with open(output_file, 'w') as f:
        f.write(str(directory) + "," + str(generation_range) + "," + str(tolerance))
        f.write("\n")


def find_and_store_criterion(directory, generation_range, start_tolerance, tolerance_step, output_file):
    """Finds and stores criterion such that all optimization runs in `directory` converge.

    Parameters
    ----------
    directory : str
        Directory holding data for possibly multiple optimization runs
    generation_range : int
        Number of generations over which variation should be computed.
    start_tolerance : float
        Start point for sweep of tolerance for convergence. Given in percentage.
    tolerance_step : float
        Step for tolerance for convergence. Given in percentage.
    output_file : str
        Name of file where to store successful convergence criterion.

    """
    tolerance = find_criterion(directory=directory, generation_range=generation_range,
                               start_tolerance=start_tolerance, tolerance_step=tolerance_step)

    store_criterion(tolerance=tolerance, generation_range=generation_range,
                    directory=directory, output_file=output_file)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--directory', type=str, help="Directory holding results of multiple optimization runs.")
    parser.add_argument('--generation_range', type=int, help="Number of generations over which to measure variation.",
                        required=False, default=15)
    parser.add_argument('--start_tolerance', type=float, help="Tolerance for convergence. Percentage.")
    parser.add_argument('--tolerance_step', type=float, help="Step to take when iterating over tolerance. Percentage.")
    parser.add_argument('--output_file', type=str, help="Name of file in which to write output.")

    args, unknown = parser.parse_known_args()
    find_and_store_criterion(directory=args.directory, generation_range=args.generation_range,
                             start_tolerance=args.start_tolerance, tolerance_step=args.tolerance_step,
                             output_file=args.output_file)
