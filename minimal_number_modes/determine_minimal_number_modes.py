import yaml
import csv
import warnings
import pandas as pd
from argparse import ArgumentParser
from decimal import Decimal
from netsquid_netconf.netconf import Loader
from netsquid_qrepchain.simulations.unified_simulation_script_qkd import run_unified_simulation_qkd
from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, process_data_bb84
from demonstrate_ga_integration.qkd.color_center.src.berlin_bonn_runscript import \
    simulation_should_be_run, extract_overall_attenuation_and_length_per_link, compute_round_times


def _update_param_file_n_modes(param_file, n_modes):
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)
    sim_params["num_modes"] = n_modes
    with open(param_file, "w") as stream:
        yaml.dump(sim_params, stream)


def _compute_success_probability(attenuation, efficiency, n_modes):
    return float(1 - (1 - 10 ** (- Decimal(attenuation) / 10) * Decimal(efficiency)) ** Decimal(n_modes))


def elementary_link_rate(config_file, param_file):
    """Computes the expected elementary link rate in the noiseless case. Simply divides the success probability of
    estabilishing entanglement by the time one attempt takes.

    The Decimal package is used as it is expected the attenuations will be such that the success probability will always
    be 0 with regular Python precision.

    Parameters
    ----------
    config_file : str
        Name of file with configuration for which simulation should be run.
    param_file : str
        Name of parameter file.

    Returns
    -------
        dataframe

    """

    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)
    attenuations, lengths = extract_overall_attenuation_and_length_per_link(config_file)
    success_prob = _compute_success_probability(attenuations[0], sim_params['efficiency'], sim_params['num_modes'])
    round_times = compute_round_times(lengths=lengths, speed_of_light=sim_params["speed_of_light"])
    expected_rate = success_prob / round_times[0]
    df = pd.DataFrame({'sk_rate': expected_rate, 'sk_error': None}, index=[0])

    return df


def find_minimal_number_of_modes(config_file_name, param_file_name, n_runs, n_modes_step_size, target_rate):
    """Determines minimal number of modes required to hit `target_rate` in the configuration defined in
    `config_file_name` and with the parameters in `param_file_name`.

    We start with only one multiplexing mode. If this is enough to hit the target rate, we return `1`. If not, we set
    `highest_number_of_modes_that_did_not_work` to `1`, increase the number of modes by `n_modes_step_size` and run
    the simulation again. From this point, there are two possibiltiies depending on the simulation outcome:

        1. If the target rate is hit, we set `lowest_n_modes_that_worked` to `n_modes` and pick a number of modes
        halfway between `highest_number_of_modes_that_did_not_work` and the current number of modes.

        2. If the target rate is not hit, we set `highest_number_of_modes_that_did_not_work` to `n_modes` and we either
        (a) increase `n_modes` by `n_modes_step_size` in case `lowest_n_modes_that_worked` is `None`, or (b) we pick a
        number of modes halfway between `n_modes` and `lowest_n_modes_that_worked`.

    The simulation is then run with the new `n_modes` and the process is repeated until `lowest_n_modes_that_worked` and
    `highest_number_of_modes_that_did_not_work` do not differ by more than `1`, at which point we return
    `lowest_n_modes_that_worked`.

    In simple terms, the algorithm employed splits the search space in half with each iteration until the minimal
    number of modes meeting the target is found.

    We note that we use the `simulation_should_be_run` method to avoid running simulations for which we can be sure that
    the performance target would not be met.

    We further note that even though the algorithm should always converge, an intelligent choice of `n_modes_step_size`
    can significantly speed up convergence. Typically, this means picking `n_modes_step_size` such that one step is
    large enough to get to a number of modes that allows for achieving the performance target, but not so large that the
    performance target is significantly surpassed. One can of course only make such a choice in an informed manner with
    prior knowledge of the performance of the configuration and parameters under study. It might then be useful to first
    perform the optimization with a small `n_runs` such that the simulations run quickly and one can get at least a
    ballpark estimate of the minimal number of modes. This should then be followed up by an optimization with a bigger
    `n_runs` and an informed choice of `n_modes_step_size`. We recommend `n_runs = 10` for the former and
    `n_runs = 1000` for the latter.

    In case we are using an elementary link, we compute the SKR analytically assuming there is no noise. This is because
    the level of attenuation in this case is such that `netsquid-magic` throws a division by zero errors irrespective of
    the parameters used. The Decimal module is used in this case for similar reasons.

    Parameters
    ----------
    config_file_name : str
        Name of file with configuration for which simulation should be run.
    param_file_name : str
        Name of parameter file.
    n_runs : int
        Number of simulation runs to perform for each number of modes.
    n_modes_step_size : int
        Size of increase in number of modes. Read docstring for more details.
    target_rate : float [Hz]
        SKR target to be met.

    Returns
    -------
    int
        Minimal number of modes.
    float
        Secret key rate achieved (Hz).
    float
        Error on secret key rate (Hz).

    """
    n_modes = 1
    lowest_n_modes_that_worked = None
    highest_n_modes_that_did_not_work = None
    n_repeaters = int(config_file_name.split("_repeater")[0])
    while True:
        _update_param_file_n_modes(param_file_name, n_modes)
        print("Currently at {} modes.".format(n_modes))
        try:
            if n_repeaters > 0 and not simulation_should_be_run(config_file=config_file_name,
                                                                param_file=param_file_name,
                                                                target_rate=target_rate):
                print("Simulation won't be run for {} modes as expected performance is below target.".format(n_modes))
                highest_n_modes_that_did_not_work = n_modes
                n_modes += n_modes_step_size
                continue
            else:
                if n_repeaters > 0:
                    repchain_df_holder, _ = run_unified_simulation_qkd(configfile=config_file_name,
                                                                       paramfile=param_file_name,
                                                                       n_runs=n_runs,
                                                                       suppress_output=True,
                                                                       max_run_duration=1.e+15,
                                                                       use_mock_data=True)
                    processed_data = \
                        process_repchain_dataframe_holder(repchain_dataframe_holder=repchain_df_holder,
                                                          processing_functions=[process_data_bb84])
                else:
                    processed_data = elementary_link_rate(config_file=config_file_name,
                                                          param_file=param_file_name)
                print("Key for {} modes was {}.".format(n_modes, processed_data["sk_rate"][0]))
                if processed_data["sk_rate"][0] >= target_rate:
                    if n_modes == 1:
                        return n_modes, processed_data["sk_rate"][0], processed_data["sk_error"][0]
                    if lowest_n_modes_that_worked is None or n_modes < lowest_n_modes_that_worked:
                        lowest_n_modes_that_worked = n_modes
                        corresponding_key, corresponding_key_error = \
                            processed_data["sk_rate"][0], processed_data["sk_error"][0]
                    if (abs(highest_n_modes_that_did_not_work - lowest_n_modes_that_worked) <= 1):
                        # statistical fluctuations on the SKR could make it so that `lowest_n_modes_that_worked` is
                        # actually smaller than `highest_n_modes_that_did_not_work`
                        if lowest_n_modes_that_worked < highest_n_modes_that_did_not_work:
                            warnings.warn("""{} modes achieved the target rate, but {} did not, likely due to
                                          statistical fluctuations. Returning {}, but consider performing more runs per
                                          data point.""".format(lowest_n_modes_that_worked,
                                                                highest_n_modes_that_did_not_work,
                                                                lowest_n_modes_that_worked))
                        return lowest_n_modes_that_worked, corresponding_key, corresponding_key_error
                    n_modes = int((highest_n_modes_that_did_not_work + lowest_n_modes_that_worked) / 2)

                elif processed_data["sk_rate"][0] < target_rate:
                    if highest_n_modes_that_did_not_work is None or n_modes > highest_n_modes_that_did_not_work:
                        highest_n_modes_that_did_not_work = n_modes
                    if lowest_n_modes_that_worked is None:
                        n_modes += n_modes_step_size
                    else:
                        new_n_modes = int((highest_n_modes_that_did_not_work + lowest_n_modes_that_worked) / 2)
                        if n_modes == new_n_modes:
                            return lowest_n_modes_that_worked, corresponding_key, corresponding_key_error
                        n_modes = new_n_modes
        except (ZeroDivisionError, ValueError) as e:
            print("""Got exception {}. This is likely because the success probability was too low. Will increase the
                  number of modes and try again.""".format(e))
            n_modes += n_modes_step_size


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('config_file', type=str, help="Name of the config file.")
    parser.add_argument('param_file', type=str, help="Name of the parameter file.")
    parser.add_argument('-n', '--n_runs', required=False, type=int, default=100,
                        help="Number of runs per configuration. If none is provided, defaults to 100.")
    parser.add_argument('--n_modes_step_size', type=int)
    parser.add_argument('--target_rate', type=int)

    args, unknown = parser.parse_known_args()
    print("Investigating minimal number of modes for configuration {}.".format(args.config_file))
    minimal_number_modes, skr, skr_error = find_minimal_number_of_modes(config_file_name=args.config_file,
                                                                        param_file_name=args.param_file,
                                                                        n_runs=args.n_runs,
                                                                        n_modes_step_size=args.n_modes_step_size,
                                                                        target_rate=args.target_rate)
    with open(args.config_file.split('.')[0] + '_performance.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['target_rate', 'minimal_n_modes', 'skr', 'skr_error', 'n_runs'])
        writer.writerow([args.target_rate, minimal_number_modes, skr, skr_error, args.n_runs])
