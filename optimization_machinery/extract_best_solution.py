import os
import csv
import yaml
import pickle
from argparse import ArgumentParser
from netsquid_netconf.netconf import Loader
from generate_configurations.generate_config_files import create_config_file_from_lookup, \
    create_lookup_table_ordered_on_asymmetry


def dump_best_solution(cost, sol, file):
    first_row = ["cost"]
    second_row = [str(cost)]
    for param in sol:
        first_row.append(str(param))
        second_row.append(str(sol[param]))
    with open(file, "w") as file:
        file.write(",".join(first_row))
        file.write("\n")
        file.write(",".join(second_row))


def determine_optimization_parameters(file):
    optimization_parameters = []
    with open(file, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        for row in reader:
            i = -2
            while True:
                if row[i] == "Qber_x_error":
                    return optimization_parameters
                optimization_parameters.append(row[i])
                i -= 1


def find_best_solution(directory, n_iters, maximum=False):
    best_cost = -1 if maximum else float("inf")
    processed_data_file = directory + "/output_0.csv"
    optimization_parameters = determine_optimization_parameters(processed_data_file)
    for i in range(0, n_iters):
        result_file_name = directory + "/csv_output_" + str(i) + ".csv"
        if i == n_iters - 1:
            result_file_name = directory + "/opt_step_" + str(i) + "/csv_output.csv"
        with open(result_file_name, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=",")
            for row in reader:
                if maximum and float(row[0]) > best_cost or not maximum and float(row[0]) < best_cost:
                    print('New best solution in iteration {}.'.format(i))
                    best_cost = float(row[0])
                    best_sol_parameters = [float(row[i]) for i in range(1, len(row))]

    return best_cost, dict(zip(optimization_parameters[::-1], best_sol_parameters))


def determine_asym_id_from_degree(table, n_repeaters, asym_degree):
    return int((len(table[n_repeaters]) - 1) * asym_degree)


def update_param_file(parameters, param_file, config_file):
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)

    for param_name, value in parameters.items():
        if param_name not in ["n_repeaters", "asym_degree"]:
            sim_params[param_name] = value
        if param_name == "cutoff_time" and value <= 1.:
            sim_params[param_name] *= sim_params["coherence_time"]

    new_param_file = config_file.split(".yaml")[0] + "_params.yaml"
    with open(new_param_file, "w") as stream:
        yaml.dump(sim_params, stream)

    return new_param_file


def config_file_from_fiber_data_and_chain_parameters(parameters, fiber_data_file, config_file_name, param_file):
    n_repeaters, asym_degree = int(parameters["n_repeaters"]), parameters["asym_degree"]
    new_param_file = update_param_file(parameters, param_file, config_file_name)
    lookup_table_filename = "rep_chain_lookup_table.pickle"
    if not os.path.isfile(lookup_table_filename):
        lookup_table = create_lookup_table_ordered_on_asymmetry(fiber_data_filename=fiber_data_file,
                                                                lookup_table_filename=lookup_table_filename)
    else:
        lookup_table = pickle.load(open(lookup_table_filename, "rb"))
    asym_id = determine_asym_id_from_degree(table=lookup_table,
                                            n_repeaters=n_repeaters,
                                            asym_degree=asym_degree)
    create_config_file_from_lookup(n_reps=n_repeaters,
                                   identifier=asym_id,
                                   lookup_table_filename=lookup_table_filename,
                                   config_file_filename=config_file_name,
                                   node_type="depolarizing_node",
                                   params_filename=new_param_file)
    return config_file_name


def generate_config_file_best_solution(maximum, n_repeaters, directory, param_file,
                                       config_file, n_iters, fiber_data_file):
    best_cost, best_sol = find_best_solution(maximum=maximum,
                                             directory=directory,
                                             n_iters=n_iters)
    dump_best_solution(best_cost, best_sol, config_file.split(".yaml")[0] + "_cost.csv")
    if n_repeaters is not None:
        print("Setting number of repeaters to {}.".format(n_repeaters))
        best_sol["n_repeaters"] = n_repeaters
    return config_file_from_fiber_data_and_chain_parameters(parameters=best_sol,
                                                            param_file=param_file,
                                                            fiber_data_file=fiber_data_file,
                                                            config_file_name=config_file)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--directory', type=str, help="Directory where to search for parameters.")
    parser.add_argument('--baselineparamfile', type=str, help="Name of the baseline parameter file.")
    parser.add_argument('--configfile', type=str, help="Desired name of output configuration file.",
                        default="a_default.yaml")
    parser.add_argument('--n_iters', type=int, help="Number of iterations in the optimization process.")
    parser.add_argument('--fiber_data_file', type=str, help="Name of the file with fiber data.",
                        default="Berlin_Bonn_fiber_data.csv")
    parser.add_argument('--n_repeaters', required=False,
                        help="In case the number of repeaters was fixed in the optimization.")
    parser.add_argument('--max', action='store_true')
    parser.set_defaults(max=False)
    args, unknown = parser.parse_known_args()

    generate_config_file_best_solution(maximum=args.max,
                                       n_repeaters=args.n_repeaters,
                                       directory=args.directory,
                                       param_file=args.baselineparamfile,
                                       config_file=args.configfile,
                                       n_iters=args.n_iters,
                                       fiber_data_file=args.fiber_data_file)
