import os
import csv
import yaml
import pickle
import fnmatch
import shutil
from argparse import ArgumentParser
from netsquid_netconf.netconf import Loader
from determine_ga_convergence import determine_convergence
from simulations.processing_function import parse_from_input_file
from generate_configurations.generate_config_files import create_config_file_from_lookup, \
    create_lookup_table_ordered_on_asymmetry
from optimization_machinery.iterate_over_criteria import find_valid_optimization_directories, \
    remove_directories_with_no_solutions, remove_non_converging_directories


def read_file(file_name):
    """Reads information regarding convergence criterion from `file_name`.

    Parameters
    ----------
    file_name : str
        Name of file to be read.

    Returns
    -------
    directory : str
        Path to directory containing optimization data.
    generation_range : int
        Range of generations over which to compute variation in convergence criterion.
    tolerance : float
        Accepted variation in convergence criterion.

    """
    with open(file_name) as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            directory = row[0]
            generation_range = int(row[1])
            tolerance = float(row[2])
    return directory, generation_range, tolerance


def find_best_solution(directory, convergence_generation):
    """Finds best solution in `directory` up to and including solutions in generation `convergence_generation`.

    Parameters
    ----------
    directory : str
        Path to directory containing results of optimization run.
    convergence_generation : int
        Generation in which convergence occurred.

    Returns
    -------
    best_sol_cost : float
        Cost of best solution found.
    best_sol : list
        List holding parameter values of best solution found.

    """
    for i in range(convergence_generation):
        sol_file = directory + "/csv_output_" + str(i) + ".csv"
        best_sol_cost = -1
        best_sol = []
        with open(sol_file, 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                sol_cost = float(row[0])
                if best_sol_cost == -1 or sol_cost < best_sol_cost:
                    best_sol = row
                    best_sol_cost = float(best_sol[0])
    return best_sol_cost, best_sol


def make_solution_dict(parameter_names, solution):
    """Creates dictionary with cost and parameters of solution from two lists, one with names of parameters and the
    other with their values and the respective cost.

    Parameters
    ----------
    parameter_names : list
        List containing names of parameters.
    soluton : list
        List containing the cost of the solution in the first entry, and the values of the parameters in the remaining.

    Returns
    -------
    dict
        Dictionary where the keys are `cost` and parameter names, and the values are the cost and the parameter values.

    """
    solution_dict = {}
    for name, value in zip(["cost"] + parameter_names, solution):
        solution_dict[name] = value
    return solution_dict


def create_parameter_file(param_dict, output_file_name, path_to_baseline_param_file):
    """Creates parameter file from baseline parameter file and a parameter dictionary. Does so by taking the baseline
    parameter file, reading into a dictionary, overwriting the values of the keys also present in `param_dict` with
    their values in `param_dict` and dumping the result into `output_file_name`.

    Parameters
    ----------
    param_dict : dict
        Dictionary where keys are parameter names and values are parameter values.
    output_file_name : str
        Name of file where to dump new parameter dictionary.
    path_to_baseline_param_file : str
        Path to baseline parameter file.

    """
    improved_param_file_name = output_file_name + '_params.yaml'
    param_dict.pop("cost")
    with open(path_to_baseline_param_file, "r") as stream:
        baseline_params = yaml.load(stream, Loader=Loader)
    for k, v in param_dict.items():
        baseline_params[k] = float(v)
    with open(improved_param_file_name, "w") as stream:
        yaml.dump(baseline_params, stream)


def find_baseline_param_file(directory):
    """Finds baseline parameter file in `directory` and returns path to it. Assumes that name of baseline parameter file
    is of the format `SOMETHINGbaseline_params.yaml`.

    Parameters
    ----------
    directory : str
        Directory where to search for baseline parameter file.

    Returns
    -------
    str
        Path to baseline parameter file.
    """
    baseline_param_file = fnmatch.filter(os.listdir(directory + "/optimization"), "*baseline_params.yaml")[0]
    return directory + "/optimization/" + baseline_param_file


def write_to_file(dictionary, output_file_name):
    """Writes dictionary to csv file, with the keys on the first row and the respective values on the second.

    Parameters
    ----------
    dictionary : dict
        Dictionary to be written.
    output_file_name : str
        File to write.
    """

    with open(output_file_name + '.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(dictionary.keys())
        csv_writer.writerow(dictionary.values())


def config_file_from_fiber_data_and_chain_parameters(parameters, fiber_data_file, config_file_name, param_file):
    n_repeaters, asym_degree = int(parameters["n_repeaters"]), float(parameters["asym_degree"])
    lookup_table_filename = "rep_chain_lookup_table.pickle"
    if not os.path.isfile(lookup_table_filename):
        lookup_table = create_lookup_table_ordered_on_asymmetry(fiber_data_filename=fiber_data_file,
                                                                lookup_table_filename=lookup_table_filename)
    else:
        lookup_table = pickle.load(open(lookup_table_filename, "rb"))
    asym_id = determine_asym_id_from_degree(table=lookup_table,
                                            n_repeaters=n_repeaters,
                                            asym_degree=asym_degree)
    create_config_file_from_lookup(n_reps=n_repeaters,
                                   identifier=asym_id,
                                   lookup_table_filename=lookup_table_filename,
                                   config_file_filename=config_file_name,
                                   node_type="depolarizing_node",
                                   params_filename=param_file)


def determine_asym_id_from_degree(table, n_repeaters, asym_degree):
    return int((len(table[n_repeaters]) - 1) * asym_degree)


def find_and_save_best_solution(input_file_name, output_file_name, fixed_config):
    """Finds best solution among the data of all optimization runs stored in a given directory which is specified in
    `input_file_name`.

    This is done by, for each optimization run, determining at what generation convergence happened. Then, the best
    solution up to and including this generation is found. This process is repeated for all optimization runs in the
    directory, with the overall best solution being the best among the best solutions of each run.

    The cost associated to the best solution as well as its parameter values are stored in a file named
    `output_file_name`. Furthermore, a `.yaml` parameter file compatible with the blueprint simulations is created with
    the name `output_file_name_params.yaml`.

    Parameters
    ----------
    input_file_name : str
        Name of file containing path to directory with optimization data and criterion for convergence.
    output_file_name : str
        Name of file where to store cost and parameter values of best solution.
    """
    directory, generation_range, tolerance = read_file(input_file_name)
    list_optimization_runs_directories = find_valid_optimization_directories(directory, "DemonstrateDeutscheTelekom",
                                                                             generation_range)
    list_optimization_runs_directories = remove_non_converging_directories(directory,
                                                                           list_optimization_runs_directories,
                                                                           10,
                                                                           15,
                                                                           1.e+100)
    list_optimization_runs_directories = remove_directories_with_no_solutions(directory,
                                                                              list_optimization_runs_directories,
                                                                              1.e+100)
    best_overall_sol_cost = -1
    for opt_directory in list_optimization_runs_directories:
        directory_to_search = directory + "/" + opt_directory + "/optimization/"
        conv_gen = determine_convergence(directory=directory + "/" + opt_directory, average_or_best="best",
                                         generation_range=generation_range, accepted_variation=tolerance)
        best_sol_cost, best_sol_params = find_best_solution(directory=directory_to_search,
                                                            convergence_generation=conv_gen)
        if best_overall_sol_cost == -1 or best_overall_sol_cost > best_sol_cost:
            best_overall_sol_cost = best_sol_cost
            best_overall_sol_params = best_sol_params

    parameter_names = parse_from_input_file(directory + "/" + list_optimization_runs_directories[0]
                                            + "/optimization/input_file.ini")
    best_overall_sol_dict = make_solution_dict(parameter_names=parameter_names,
                                               solution=best_overall_sol_params)
    path_to_baseline_param_file = find_baseline_param_file(
        directory=directory + "/" + list_optimization_runs_directories[0])
    write_to_file(dictionary=best_overall_sol_dict,
                  output_file_name=output_file_name)
    create_parameter_file(param_dict=best_overall_sol_dict,
                          output_file_name=output_file_name,
                          path_to_baseline_param_file=path_to_baseline_param_file)
    if not fixed_config:
        config_file_from_fiber_data_and_chain_parameters(parameters=best_overall_sol_dict,
                                                         fiber_data_file="../generate_configurations/"
                                                                         "Berlin_Bonn_fiber_data.csv",
                                                         config_file_name=output_file_name + "_config.yaml",
                                                         param_file=output_file_name + "_params.yaml")
    else:
        config_file_path = \
            directory + "/" + list_optimization_runs_directories[0] + "/optimization/best_10_Hz_config.yaml"
        shutil.copyfile(config_file_path, output_file_name + "_config.yaml")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--input_file_name', type=str, help="""Name of file with relevant data. Must include variation
    tolerance, generation range and directory.""")
    parser.add_argument('--output_file_name', type=str, help="Name of file where to store best solution.")
    parser.add_argument('--fixed_config', action='store_true')
    parser.add_argument('--no-fixed_config', dest='fixed_config', action='store_false')
    parser.set_defaults(fixed_config=False)

    args, unknown = parser.parse_known_args()
    find_and_save_best_solution(input_file_name=args.input_file_name,
                                output_file_name=args.output_file_name,
                                fixed_config=args.fixed_config)
