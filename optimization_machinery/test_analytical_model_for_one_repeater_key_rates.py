from optimization_machinery.analytical_model_for_one_repeater_key_rates import \
    _NumberOfRoundsWithoutCutoff, _NumberOfRoundsWithinCutoff, _distribution_time
import unittest
import numpy as np


def test_geometric_variable(p=0.01, base=0.6):

    geom = _NumberOfRoundsWithoutCutoff(link_success_prob=p)
    assert geom.expectation_value == 1 / p
    assert geom.expectation_value_of_exponential(base=base) == base * p / (1 - base * (1 - p))


class TestTruncatedGeometricVariable(unittest.TestCase):
    p = 0.01
    base = 0.6

    def test_large_cutoff(self):
        """For large cutoff, truncated geometric distribution becomes approximately a normal geometric again."""

        var = _NumberOfRoundsWithinCutoff(link_success_prob=self.p, cutoff_round=int(1E4))
        exp_val = var.expectation_value
        assert exp_val < 1 / self.p  # should always be smaller
        assert exp_val > (1 - 0.0001) / self.p  # should be close though
        assert np.isclose(var.expectation_value_of_exponential(base=self.base),
                          self.base * self.p / (1 - self.base * (1 - self.p)))

    def test_small_cutoff(self):
        """For cutoff_round = 1, it must succeed on first attempt; thus, Pr(n = i) = delta_{i, 1}."""

        var = _NumberOfRoundsWithinCutoff(link_success_prob=self.p, cutoff_round=1)
        assert np.isclose(var.normalization_constant, self.p)
        assert np.isclose(var.expectation_value, 1)
        assert np.isclose(var.expectation_value_of_exponential(base=self.base), self.base)


class TestDistributionTime(unittest.TestCase):
    round_time = 4
    p = 0.01

    def distr_time(self, cutoff_round):
        if cutoff_round is None:
            var1 = None
        else:
            var1 = _NumberOfRoundsWithinCutoff(link_success_prob=self.p, cutoff_round=cutoff_round)
        var2 = _NumberOfRoundsWithoutCutoff(link_success_prob=self.p)
        return _distribution_time(round_time=self.round_time,
                                  number_of_rounds_within_cutoff=var1,
                                  number_of_rounds_without_cutoff=var2)

    def test_no_cutoff(self):
        distr_time = self.distr_time(cutoff_round=None)
        assert distr_time == self.round_time * 2 / self.p

    def test_large_cutoff(self):
        distr_time = self.distr_time(cutoff_round=int(1E4))
        assert np.isclose(distr_time, self.round_time * 2 / self.p)

    def test_small_cutoff(self):
        distr_time = self.distr_time(cutoff_round=1)

        prob_succeeding_within_cutoff = self.p
        exp_val_nmin = 1  # can only succeed first round
        exp_val_nplus = 1 / self.p  # this is a slight underestimate
        expected_distr_time = 1 / self.p + (1 / prob_succeeding_within_cutoff - 1) * exp_val_nplus + exp_val_nmin
        expected_distr_time *= self.round_time
        assert expected_distr_time < distr_time
        assert np.isclose(expected_distr_time, distr_time, rtol=2 * self.p)
