from generate_configurations.generate_config_files import extract_fiber_data, determine_all_chain_configurations, \
    _remove_all_nodes_except_one, _place_midpoint, create_config_file, create_lookup_table_ordered_on_asymmetry, \
    create_config_file_from_lookup, _number_of_nodes_in_repeater_chain, _maximum_number_of_repeaters_in_fiber_path
import netsquid as ns
from generate_configurations.fiber_path import FiberPath
from netsquid_qrepchain.processing_nodes.nodes_with_drivers import AbstractNodeWithDriver, TINodeWithDriver, \
    NVNodeWithDriver, DepolarizingNodeWithDriver
from netsquid_qrepchain.control_layer.swapasap_egp import SwapAsapEndNodeLinkLayerProtocol
from netsquid_qrepchain.simulations.unified_simulation_script_state import run_simulation
from netsquid_physlayer.heralded_connection import MiddleHeraldedConnection, HeraldedConnection
from netsquid_magic.abstract_heralded_connection import AbstractHeraldedConnection, AbstractHeraldedMagic
from netsquid_physlayer.classical_connection import ClassicalConnectionWithLength
from netsquid_netconf.netconf import ComponentBuilder, netconf_generator
from netsquid_driver.EGP import EGPService
import pandas as pd
import pickle
import os
from qlink_interface import ReqCreateAndKeep, ResCreateAndKeep
from netsquid.qubits.qubitapi import reduced_dm

mock_lengths = [10, 20, 30]
mock_attenuation_coefficients = [0.1, 0.2, 0.3]
mock_labels = ["node_1", "node_2", "node_3", "node_4"]
# node_1 -10- node_2 -20- node_3 -30- node_4
# note: the node_index of node_2 is 1, and of node_3 is 2 (not defined for the others)
mock_fiber_data = {"node_a": mock_labels[:-1],
                   "node_b": mock_labels[1:],
                   "length": mock_lengths,
                   "attenuation_coefficient": mock_attenuation_coefficients}


class EGPDataCollectorBothNodes:
    """EGP data collector that collects state data from both end nodes. Used to test that qubit ordering is as expected.
    """
    def __init__(self, start_time=ns.sim_time()):
        # probably this in not necessary
        self.sim_time = start_time

    def __call__(self, evexpr):
        """Picks up responses from the EGP when they are signalled by both Protocols.
        """
        generation_time = ns.sim_time() - self.sim_time
        self.sim_time = ns.sim_time()

        response_alice = evexpr.first_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.first_term.triggered_events[0])
        response_bob = evexpr.second_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.second_term.triggered_events[0])

        assert evexpr.first_term.triggered_events[0].source != evexpr.second_term.triggered_events[0].source

        [alice_qubit] = evexpr.first_term.triggered_events[0].source.node. \
            qmemory.peek(positions=response_alice.result.logical_qubit_id, skip_noise=False)
        [bob_qubit] = evexpr.second_term.triggered_events[0].source.node. \
            qmemory.peek(positions=response_bob.result.logical_qubit_id, skip_noise=False)
        state_alice = reduced_dm([alice_qubit, bob_qubit])
        state_bob = reduced_dm([bob_qubit, alice_qubit])

        data = {"state_alice": state_alice,
                "state_bob": state_bob,
                "generation_duration": generation_time * 1E-9,
                "midpoint_outcome_0_alice": response_alice.result.bell_state,
                "midpoint_outcome_0_bob": response_bob.result.bell_state
                }

        return data


def implement_magic(network):
    """Add magic distributors to a physical network.

    To every connection in the network, a magic distributor is added as attribute.
    This is always a :class:`netsquid_magic.abstract_heralded_connection.AbstractHeraldedMagic`.
    The magic distributor is given the connection as argument upon construction (so that parameters to be used in magic
    can be read from the connection).

    Parameters
    ----------
    network : :class:`netsquid.nodes.network.Network`
        Network that the network distributors should be added to.

    """

    for connection in network.connections.values():
        if isinstance(connection, AbstractHeraldedConnection):
            nodes = [port.connected_port.component for port in connection.ports.values()]
            magic_distributor = AbstractHeraldedMagic(nodes=nodes, heralded_connection=connection)
            connection.magic_distributor = magic_distributor


def setup_networks(config_file_name):
    """
    Set up network(s) according to configuration file

    Parameters
    ----------
    config_file_name : str
        Name of configuration file

    Returns
    -------
    generator : generator
        Generator yielding network configurations
    """

    # add required components to ComponentBuilder
    ComponentBuilder.add_type(name="abstract_node", new_type=AbstractNodeWithDriver)
    ComponentBuilder.add_type(name="ti_node", new_type=TINodeWithDriver)
    ComponentBuilder.add_type(name="nv_node", new_type=NVNodeWithDriver)
    ComponentBuilder.add_type(name="depolarizing_node", new_type=DepolarizingNodeWithDriver)
    ComponentBuilder.add_type(name="heralded_connection", new_type=HeraldedConnection)
    ComponentBuilder.add_type(name="mid_heralded_connection", new_type=MiddleHeraldedConnection)
    ComponentBuilder.add_type(name="classical_connection", new_type=ClassicalConnectionWithLength)

    generator = netconf_generator(config_file_name)
    return generator


def setup_protocols(network):
    """Setup any protocols that are not set up using netconf by :func:`setup_network`. Also starts all services.

    Most protocols should already be set in the driver of the nodes in the network.
    It is here assumed that the only protocols that are not yet set up, are the link-layer protocols
    (since these need global information about the network when constructed).
    All services known to the driver of each node in the network are started afterwards.

    Parameters
    ----------
    network : :class:`netsquid.nodes.network.Network`
        Network on which protocols should be set up.

    """

    # repeaters have 4 ports: two communication ports, two entanglement ports
    repeaters = [node for node in network.nodes.values() if len(node.ports) == 4]

    # end nodes have 2 ports: one communication port, one entanglement port
    end_nodes = [node for node in network.nodes.values() if len(node.ports) == 2]

    # check that there are only 2 end nodes
    assert len(end_nodes) == 2

    # check that each node is either a repeater node or an end node
    assert len(repeaters) + len(end_nodes) == len(network.nodes)

    # start all protocols on repeaters
    for repeater in repeaters:
        repeater.driver.start_all_services()

    # add link-layer protocol to end nodes and start all protocols
    for end_node in end_nodes:
        end_node.driver.add_service(EGPService, SwapAsapEndNodeLinkLayerProtocol(node=end_node))
        end_node.driver.start_all_services()


def magic_and_protocols(objects):
    network = objects["network"]
    implement_magic(network)
    setup_protocols(network)
    end_nodes = [node for node in network.nodes.values() if len(node.ports) == 2]
    egp_services = [end_node.driver[EGPService] for end_node in end_nodes]
    return egp_services, network


def create_and_keep(config_file_name):
    ns.sim_reset()
    generator = setup_networks(config_file_name)
    objects, config = next(generator)
    egp_services, network = magic_and_protocols(objects=objects)
    data = run_simulation(egp_services=egp_services,
                          request_type=ReqCreateAndKeep,
                          response_type=ResCreateAndKeep,
                          n_runs=10)
    assert set(data.columns) == {"state", "generation_duration", "midpoint_outcome_0"}


def test_extract_fiber_data(filename="test_fiber_data"):
    pd.DataFrame(mock_fiber_data).to_csv(filename, index=False)
    fiber_data = extract_fiber_data(filename)
    assert fiber_data.lengths == mock_lengths
    assert fiber_data.attenuation_coefficients == mock_attenuation_coefficients
    assert fiber_data.labels == mock_labels
    os.remove(filename)


def test_determine_all_chain_configurations():
    # There are no configurations if there is only one segment; no space for heralding station
    assert len(determine_all_chain_configurations(FiberPath(lengths=[10], attenuation_coefficients=[0.2],
                                                            labels=["start", "end"]),
                                                  place_midpoints_in_center=True)) == 0
    # With two fiber segments, the only option is to add a heralding station
    assert len(determine_all_chain_configurations(FiberPath(lengths=[10, 20], attenuation_coefficients=[0.2, 0.4],
                                                            labels=["start", "mid", "end"]),
                                                  place_midpoints_in_center=True)) == 1
    # With three fiber segments, there is still not enough space for a repeater
    assert len(determine_all_chain_configurations(FiberPath(lengths=[10, 20, 10],
                                                            attenuation_coefficients=[0.2, 0.4, 0.6],
                                                            labels=["start", "mid1", "mid2", "end"]),
                                                  place_midpoints_in_center=True)) == 1
    # With four fiber segments, we can finally put a repeater; there should be two options now!
    assert len(determine_all_chain_configurations(FiberPath(lengths=[10, 20, 10, 30],
                                                            attenuation_coefficients=[0.2, 0.4, 0.6, 0.3],
                                                            labels=["start", "mid1", "mid2", "mid3", "end"]),
                                                  place_midpoints_in_center=True)) == 2
    # with 12 nodes (10 nodes if you don't count end nodes), the number of placements should be the 10th Fibonacci
    # number, which is 55
    assert len(determine_all_chain_configurations(FiberPath(lengths=[10] * 11,
                                                            attenuation_coefficients=[0.1] * 11,
                                                            labels=["some_label"] * 12),
                                                  place_midpoints_in_center=True)) == 55
    # Check that attenuation coefficients are calculated correctly
    # For the mock data, if the heralding station is as central as possible, it should be E - 0 - H - E.
    avg_att_left = sum([mock_attenuation_coefficients[i] * mock_lengths[i] for i in range(len(mock_lengths) - 1)])
    length_left = sum([length for length in mock_lengths[:-1]])
    avg_att_left /= length_left
    expected_labels = [mock_labels[0], mock_labels[2], mock_labels[3]]
    [fiber_path] = determine_all_chain_configurations(FiberPath(lengths=mock_lengths,
                                                                attenuation_coefficients=mock_attenuation_coefficients,
                                                                labels=mock_labels), place_midpoints_in_center=True)
    assert fiber_path.lengths == [length_left, mock_lengths[-1]]
    assert fiber_path.attenuation_coefficients == [avg_att_left, mock_attenuation_coefficients[-1]]
    assert fiber_path.labels == expected_labels


def test_place_midpoint():
    [fiber_path] = _place_midpoint(FiberPath(lengths=mock_lengths,
                                             attenuation_coefficients=mock_attenuation_coefficients,
                                             labels=mock_labels), place_midpoint_at_center=True)
    assert fiber_path.num_nodes == 3
    assert fiber_path.num_segments == 2
    assert fiber_path.lengths == [30, 30]
    assert fiber_path.labels == ["node_1", "node_3", "node_4"]
    # placing the midpoint at node_3 puts it exactly in the center


def test_remove_all_nodes_except_one():
    fiber_path = _remove_all_nodes_except_one(FiberPath(lengths=mock_lengths,
                                                        attenuation_coefficients=mock_attenuation_coefficients,
                                                        labels=mock_labels), node_index=2)
    assert fiber_path.num_nodes == 3
    assert fiber_path.num_segments == 2
    assert fiber_path.lengths == [30, 30]
    assert fiber_path.labels == ["node_1", "node_3", "node_4"]


def test_create_lookup_table_ordered_on_asymmetry(fiber_path_filename="test_lookup_table_path.csv",
                                                  table_filename="test_lookup_table.pickl"):
    pd.DataFrame(mock_fiber_data).to_csv(fiber_path_filename, index=False)
    table_returned = create_lookup_table_ordered_on_asymmetry(fiber_data_filename=fiber_path_filename,
                                                              lookup_table_filename=table_filename)
    table_saved = pickle.load(open(table_filename, "rb"))
    for table in [table_returned, table_saved]:
        assert len(table) == 1  # only 0 repeaters possible
        assert len(table[0]) == 1  # exactly one way to place 0 repeaters
        assert isinstance(table[0][0], FiberPath)  # the config is actually a fiber path

    # end node -20- node -20- node -8- node -12- node -9- node -11- end node
    lengths = [20, 20, 8, 12, 9, 11]
    attenuation_coefficients = [0.1] * 6
    labels = [f"node_{i}" for i in range(7)]
    fiber_data = {"node_a": labels[:-1],
                  "node_b": labels[1:],
                  "length": lengths,
                  "attenuation_coefficient": attenuation_coefficients}
    pd.DataFrame(fiber_data).to_csv(fiber_path_filename, index=False)
    table_returned = create_lookup_table_ordered_on_asymmetry(fiber_data_filename=fiber_path_filename,
                                                              lookup_table_filename=table_filename)
    table_saved = pickle.load(open(table_filename, "rb"))
    for table in [table_returned, table_saved]:
        assert len(table) == 3  # either no repeater, one repeater, or two repeaters
        assert len(table[0]) == 1  # one way to place zero repeaters
        assert len(table[1]) == 3  # three ways to place one repeater
        assert len(table[2]) == 1  # one way to place two repeaters
        assert table[1][0].lengths == [20, 20, 20, 20]  # most symmetric choice
        assert table[1][1].lengths == [20, 28, 12, 20]  # second symmetric choice
        assert table[1][2].lengths[-2:] == [9, 11]  # second symmetric choice
        # (first midpoint has two equivalently symmetric choices, so we don't test that)

    os.remove(table_filename)
    create_lookup_table_ordered_on_asymmetry(fiber_data_filename=fiber_path_filename, lookup_table_filename=None)
    assert not os.path.isfile(table_filename)
    os.remove(fiber_path_filename)


def test_number_of_nodes_in_repeater_chain():
    assert _number_of_nodes_in_repeater_chain(0) == 3
    assert _number_of_nodes_in_repeater_chain(1) == 5
    assert _number_of_nodes_in_repeater_chain(10) == 23
    assert type(_number_of_nodes_in_repeater_chain(4)) is int


def test_maximum_number_of_repeaters_in_fiber_path():
    assert _maximum_number_of_repeaters_in_fiber_path(3) == 0
    assert _maximum_number_of_repeaters_in_fiber_path(4) == 0
    assert _maximum_number_of_repeaters_in_fiber_path(5) == 1
    assert _maximum_number_of_repeaters_in_fiber_path(6) == 1
    assert type(_maximum_number_of_repeaters_in_fiber_path(20)) is int


def test_create_config_file(filename="test_create_config_file_from_fiber_path.yaml"):
    fiber_path = FiberPath(lengths=mock_lengths + [15], attenuation_coefficients=mock_attenuation_coefficients + [0.15],
                           labels=mock_labels + ["node_5"])
    ComponentBuilder.add_type(new_type=AbstractHeraldedConnection, name="abstract_heralded_connection")
    create_config_file(fiber_path=fiber_path, filename=filename,
                       params_filename="generate_configurations/params_for_test_generate_config_files.yaml",
                       node_type="abstract_node")
    create_and_keep(filename)
    os.remove(filename)


def test_create_config_file_from_lookup(fiber_path_filename="test_lookup_table_path.csv",
                                        table_filename="test_lookup_table_pickle"):
    pd.DataFrame(mock_fiber_data).to_csv(fiber_path_filename, index=False)
    create_lookup_table_ordered_on_asymmetry(fiber_data_filename=fiber_path_filename,
                                             lookup_table_filename=table_filename)
    create_config_file_from_lookup(0, 0, table_filename, config_file_filename="test_config_filename.yaml",
                                   node_type="test_node", params_filename=None)
    os.remove(fiber_path_filename)
    os.remove(table_filename)
    os.remove("test_config_filename.yaml")
