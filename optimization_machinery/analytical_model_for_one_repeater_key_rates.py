from abc import ABCMeta, abstractmethod
import numpy as np
import math
from netsquid_simulationtools.process_qkd import _estimate_bb84_secret_key_rate
from argparse import ArgumentParser

"""
Code to evaluate an analytical model for measure-directly protocol with cutoff time
on a single sequential repeater in a symmetric setup,
where the only source of noise is depolarizing noise on the repeater's memory.
All classical communication is neglected in these calculations.
"""


def analyze_bb84_performance_double_click(length, coherence_time, cutoff_time=None, speed_of_light=200000,
                                          attenuation=0.25, only_dephasing=False, measure_directly=True):
    """Evaluate performance of BB84 protocol on symmetric setup of one sequential repeater with depolarizing memory
    and using double-click protocol.

    Length combined with speed of light is used to calculate the duration of each attempt at entanglement generation.
    Length combined with attenuation coefficient is used to calculate the success probability of each attempt at
    entanglement generation.
    Because double-click is used, the success probability per attempt is set to the transmittance of the fiber
    divided by two (representing a linear-optics Bell-state measurement).
    Here, by the transmittance of the fiber we mean the transmittance of the fiber between the repeater and an
    end node, not between a node and the midpoint station where the linear-optics Bell-state measurement is performed.

    Parameters
    ----------
    length : float
        Length of fiber between repeater and each end node.
        Thus, the total length between the end nodes is twice the value of this argument.
    coherence_time : float
        Coherence time [s] of the memory at the quantum repeater.
        Depolarizing noise on quantum memories is applied with parameter exp(- t / coherence_time).
    cutoff_time : float or None
        Time [s] after which entangled qubits are discarded from memory.
        If None, no cutoff is used.
    speed_of_light : float
        Speed [km/s] of light in fiber.
    attenuation : float
        Attenuation coefficient [dB/km] of fiber.
    only_dephasing : bool
        True if noise is dephasing instead of depolarizing (which includes both dephasing and bit flips).
    measure_directly : bool
        True if qubits are measured directly at end nodes.
        Setting this to False doubles the total decoherence time of the entangled pair,
        since now both qubits of the entangled pair are stored in memory.

    Returns
    -------
    secret_key_rate : float
        Secret-key rate achievable in BB84.
    qber : float
        QBER of BB84. Used to calculate secret-key rate.
    distribution_time : float
        Time [s] required per distributed entangled pair. Used to calculate secret-key rate.

    """
    link_success_prob = _link_success_prob(length=length, attenuation=attenuation)
    round_time = _round_time(length=length, speed_of_light=speed_of_light)
    return analyze_bb84_performance(link_success_prob=link_success_prob, round_time=round_time,
                                    coherence_time=coherence_time, cutoff_time=cutoff_time,
                                    only_dephasing=only_dephasing, measure_directly=measure_directly)


def analyze_bb84_performance(link_success_prob, round_time, coherence_time, cutoff_time=None, only_dephasing=False,
                             measure_directly=True):
    """Evaluate performance of BB84 protocol on symmetric setup of one sequential repeater with depolarizing memory.

    Parameters
    ----------
    link_success_prob : float
        Probability of succeeding at entanglement generation per attempt.
    round_time : float
        Time [s] required to perform a single attempt at entanglement generation.
    coherence_time : float
        Coherence time [s] of the memory at the quantum repeater.
        Depolarizing noise on quantum memories is applied with parameter exp(- t / coherence_time).
    cutoff_time : float or None
        Time [s] after which entangled qubits are discarded from memory.
        If None, no cutoff is used.
    only_dephasing : bool
        True if noise is dephasing instead of depolarizing.
    measure_directly : bool
        True if qubits are measured directly at end nodes.
        Setting this to False doubles the total decoherence time of the entangled pair,
        since now both qubits of the entangled pair are stored in memory.

    Returns
    -------
    secret_key_rate : float
        Secret-key rate achievable in BB84.
    qber : float
        QBER of BB84. Used to calculate secret-key rate.
    distribution_time : float
        Time [s] required per distributed entangled pair. Used to calculate secret-key rate.

    """
    number_of_rounds_without_cutoff = _NumberOfRoundsWithoutCutoff(link_success_prob=link_success_prob)
    if cutoff_time is None:
        qber = _qber(stochastic_variable=number_of_rounds_without_cutoff, round_time=round_time,
                     coherence_time=coherence_time)
        distribution_time = _distribution_time(round_time=round_time,
                                               number_of_rounds_without_cutoff=number_of_rounds_without_cutoff,
                                               number_of_rounds_within_cutoff=None)
    else:
        cutoff_round = math.floor(cutoff_time / round_time)
        if cutoff_round < 1:
            raise ValueError("Cutoff time cannot be smaller than round time.")
        number_of_rounds_within_cutoff = _NumberOfRoundsWithinCutoff(link_success_prob=link_success_prob,
                                                                     cutoff_round=cutoff_round)
        qber = _qber(stochastic_variable=number_of_rounds_within_cutoff, round_time=round_time,
                     coherence_time=coherence_time, measure_directly=measure_directly)
        distribution_time = _distribution_time(round_time=round_time,
                                               number_of_rounds_without_cutoff=number_of_rounds_without_cutoff,
                                               number_of_rounds_within_cutoff=number_of_rounds_within_cutoff)
    qber_x = qber
    qber_z = qber if not only_dephasing else 0
    secret_key_rate, _, _, _ = _estimate_bb84_secret_key_rate(qber_x=qber_x, qber_z=qber_z,
                                                              attempts_per_success=distribution_time,
                                                              qber_x_error=0, qber_z_error=0,
                                                              attempts_per_success_error=0)
    return secret_key_rate, qber, distribution_time


def _qber(stochastic_variable, round_time, coherence_time, measure_directly=True):
    """Quantum-Bit Error Rate of BB84. Errors are due to depolarizing noise on repeater memory.

    Parameters
    ----------
    stochastic_variable : :class:`_StochasticVariable`
        Stochastic variable representing the number of rounds a qubit is kept in memory at the repeater before
        it is successfully swapped.
        This is a simple geometric distribution in the absence of a cutoff time,
        or a truncated geometric distribution if there is a cutoff time.
    round_time : float
        Time [s] required to perform a single attempt at entanglement generation.
    coherence_time : float
        Coherence time [s] of the memory at the quantum repeater.
        Depolarizing noise on quantum memories is applied with parameter exp(- t / coherence_time).
    measure_directly : bool
        True if qubits are measured directly at end nodes.
        Setting this to False doubles the total decoherence time of the entangled pair,
        since now both qubits of the entangled pair are stored in memory.

    Returns
    -------
    float
        The QBER.

    """
    if measure_directly:
        base = np.exp(- round_time / coherence_time)
    else:
        base = np.exp(- 2 * round_time / coherence_time)
    return (1 - stochastic_variable.expectation_value_of_exponential(base=base)) / 2


def _distribution_time(round_time, number_of_rounds_without_cutoff, number_of_rounds_within_cutoff=None):
    """Average time to distribute a single entangled pair.

    Parameters
    ----------
    round_time : float
        Time [s] required to perform a single attempt at entanglement generation.
    number_of_rounds_without_cutoff : :class:`_NumberOfRoundsWithoutCutoff`
        Stochastic variable representing how long it takes to generate a single elementary link without cutoff.
        This is a geometrically-distributed variable.
    number_of_rounds_within_cutoff : :class:`_NumberOfRoundsWithinCutoff` or None
        Stochastic variable representing how long it takes to generate a single elementary link,
        conditioned on it succeeding within a certain cutoff time.
        This variable is distributed according to a truncated geometric distribution.
        Use None if there is no cutoff time.

    Returns
    -------
    float
        Average distribution time per entangled pair.

    """
    distribution_time_in_rounds = \
        _distribution_time_in_number_of_rounds(number_of_rounds_without_cutoff=number_of_rounds_without_cutoff,
                                               number_of_rounds_within_cutoff=number_of_rounds_within_cutoff)
    return round_time * distribution_time_in_rounds


def _distribution_time_in_number_of_rounds(number_of_rounds_without_cutoff, number_of_rounds_within_cutoff=None):
    """Average number of rounds of elementary-link entanglement distribution to get end-to-end entanglement.

    Parameters
    ----------
    number_of_rounds_without_cutoff : :class:`_NumberOfRoundsWithoutCutoff`
        Stochastic variable representing how long it takes to generate a single elementary link without cutoff.
        This is a geometrically-distributed variable.
    number_of_rounds_within_cutoff : :class:`_NumberOfRoundsWithinCutoff`
        Stochastic variable representing how long it takes to generate a single elementary link,
        conditioned on it succeeding within a certain cutoff time.
        This is a truncated geometric distribution.

    Returns
    -------
    int
        Average number of rounds per entangled pair.

    """
    if number_of_rounds_within_cutoff is None:
        return 2 * number_of_rounds_without_cutoff.expectation_value
    return (number_of_rounds_without_cutoff.expectation_value
            * (1 + 1 / number_of_rounds_within_cutoff.normalization_constant))


def _link_success_prob(length, attenuation=0.25):
    """Success probability for a single elementary-link entanglement-distribution attempt.

    Assumes success probability is transmittance / 2, where transmittance is the transmission probability
    of the fiber between two nodes (not of the fiber between a node and a midpoint station).
    This corresponds to using a double-click scheme with a 50% success probability Bell-state measurement at the
    midpoint.

    Parameters
    ---------
    length : float
        Length of fiber between repeater and each end node.
        Thus, the total length between the end nodes is twice the value of this argument.
    attenuation : float
        Attenuation coefficient [dB/km] of fiber.

    Returns
    -------
    float
        Elementary-link success probability.

    """
    return np.power(10, - length / 10 * attenuation) / 2


def _round_time(length, speed_of_light=200000):
    """Time it takes to perform a single round of elementary-link entanglement distribution.

    Parameters
    ----------
    length : float
        Length of fiber between repeater and each end node.
        Thus, the total length between the end nodes is twice the value of this argument.
    speed_of_light : float
        Speed [km/s] of light in fiber.

    Returns
    -------
    float
        Time it takes to perform a single round of elementary-link entanglement distribution.

    """
    return length / speed_of_light


class _StochasticVariable(metaclass=ABCMeta):
    """Class representing a stochastic variable, i.e. a variable with a probability distribution for its value."""

    def __init__(self):
        self._expectation_value = None

    @property
    def expectation_value(self):
        """Expectation value of the stochastic variable, i.e. <n>, where n is the stochastic variable.

        Returns
        -------
        float
            Expectation value.

        """
        if self._expectation_value is None:
            self._expectation_value = self._calculate_expectation_value()
        return self._expectation_value

    @abstractmethod
    def _calculate_expectation_value(self):
        """Calculate expectation value.

        Returns
        -------
        float
            Expectation value.

        """
        pass

    @abstractmethod
    def expectation_value_of_exponential(self, base):
        """Calculate <a^n>, where n is the stochastic variable and a is the number `base`.

        Parameters
        ----------
        base : float
            The number a in the expression <a^n> that is to be evaluated.

        Returns
        -------
        float
            Expectation value of the exponential.

        """
        pass


class _NumberOfRoundsWithoutCutoff(_StochasticVariable):
    """Class representing stochastic variable: number of rounds until success, in absence of cutoff.

    This is a simple geometric variable.

    Parameters
    ----------
    link_success_prob : float
        Success probability per attempt at elementary-link entanglement generation.

    """
    def __init__(self, link_success_prob):
        self.link_success_prob = link_success_prob
        super().__init__()

    def _calculate_expectation_value(self):
        """Calculate expectation value.

        Returns
        -------
        float
            Expectation value.

        """
        return 1 / self.link_success_prob

    def expectation_value_of_exponential(self, base):
        """Calculate <a^n>, where n is the stochastic variable and a is the number `base`.

        To calculate this quantity, the formula for geometric series can be used.
        Therefore, the infinite sum can be resolved exactly, and no explicit summation is required.

        Parameters
        ----------
        base : float
            The number a in the expression <a^n> that is to be evaluated.

        Returns
        -------
        float
            Expectation value of the exponential.

        """
        return base * self.link_success_prob / (1 - base * (1 - self.link_success_prob))


class _NumberOfRoundsWithinCutoff(_StochasticVariable):
    """Class representing stochastic variable: number of rounds until success, given it's within the cutoff.

    This is a truncated geometric variable.
    That is, it's probability distribution is the probability distribution of a geometric variable,
    but with Pr(n = x) = 0 for x > cutoff_round.
    The remaining probability distribution is then renormalized (using the normalization constant
    `_NumberOfRoundsWithinCutoff.normalization_constant`).

    Parameters
    ----------
    link_success_prob : float
        Success probability per attempt at elementary-link entanglement generation.
    cutoff_round : int
        Maximum number of attempts.
        If success is obtained after this number of rounds, it is still accepted.
        I.e. the probability distribution is only put to zero for x > cutoff_round, not for x = cutoff_round.

    """
    def __init__(self, link_success_prob, cutoff_round):
        if not isinstance(cutoff_round, int):
            if cutoff_round.is_integer():
                cutoff_round = int(cutoff_round)
            else:
                raise TypeError
        self.link_success_prob = link_success_prob
        self.cutoff_round = cutoff_round
        self._normalization_constant = None
        super().__init__()

    @property
    def normalization_constant(self):
        """Constant used to normalize this probability distribution.

        The value is sum_{i=1}^{cutoff} (1 - p)^{i - 1} p,
        where p is the success probability per attempt.

        Returns
        -------
        float
            Normalization constant.

        """
        if self._normalization_constant is None:
            self._normalization_constant = self._calculate_normalization_constant()
        return self._normalization_constant

    def _calculate_expectation_value(self):
        """Calculate expectation value.

        This is done by explicitly summing over the probability distribution until the cutoff is reached.

        Returns
        -------
        float
            Expectation value.

        """
        value = 0
        for index in range(1, self.cutoff_round + 1):
            value += index * (1 - self.link_success_prob) ** (index - 1)
        return value * self.link_success_prob / self.normalization_constant

    def expectation_value_of_exponential(self, base):
        """Calculate <a^n>, where n is the stochastic variable and a is the number `base`.

        The summation <a^n> that needs to be evaluated can be written as
        a * p / N- * sum_{i=0}^{cutoff - 1} [a * (1 - p)] ** i,
        which can be evaluated using the standard formula for geometric series as
        a * p / N- * (1 - [a * (1 - p) ] ** cutoff) / (1 - a * (1 - p)).
        Note: N- is the normalization constant of the distribution.

        Parameters
        ----------
        base : float
            The number a in the expression <a^n> that is to be evaluated.

        Returns
        -------
        float
            Expectation value of the exponential.

        """
        value = base * self.link_success_prob / self.normalization_constant
        value *= 1 - (base * (1 - self.link_success_prob)) ** self.cutoff_round
        value /= 1 - base * (1 - self.link_success_prob)
        return value

    def _calculate_normalization_constant(self):
        """Calculate the normalization constant.

        Returns
        -------
        float
            Normalization constant.

        """
        return 1 - (1 - self.link_success_prob) ** self.cutoff_round


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("--length", "-l", type=float, required=True)
    parser.add_argument("--coherence_time", "-coh", type=float, required=True)
    parser.add_argument("--cutoff_time", "-cut", type=float, required=False, default=None)
    parser.add_argument("--only_dephasing", dest="only_dephasing", action="store_true",
                        help="Use dephasing noise instead of depolarizing noise.")
    parser.add_argument("--do_not_measure_directly", dest="measure_directly", action="store_false", default=True,
                        help="Do not measure qubits directly at end nodes, but store until end-to-end entanglement"
                             "is confirmed.")
    args = parser.parse_args()
    skr, qber, distribution_time = analyze_bb84_performance_double_click(length=args.length,
                                                                         coherence_time=args.coherence_time,
                                                                         cutoff_time=args.cutoff_time,
                                                                         only_dephasing=args.only_dephasing,
                                                                         measure_directly=args.measure_directly)
    print(f"For\n\nlength = {args.length} km,\ncoherence time = {args.coherence_time} s,\n"
          f"cutoff time = {args.cutoff_time} s,"
          f"\n\nwe find\n\nSKR={skr} Hz,\nqber={qber},\ndistribution time = {distribution_time} s.")
