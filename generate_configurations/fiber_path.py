import numpy as np


class FiberPath:
    """A fiber path, made up of segments with associated loss coefficients and labels for the nodes in between.

    Parameters
    ----------
    lengths : list of float
        Lengths [km] of the fiber segments in the fiber data.
        `lengths[i]` is the length of the (i+1)th segment.
    attenuation_coefficients : list of float
        Attenuation coefficients [db/km] of the fiber segments in the fiber data. Same list length as `lengths`.
        `attenuation_coefficients` is the attenuation coefficient of the (i+1)th segment.
    labels : list of str
        Labels (or names) of the nodes of the fiber path. List length is length of `lengths` plus one.
        `labels[i]` is the label of the (i+1)th node.

    """
    def __init__(self, lengths, attenuation_coefficients, labels):
        self._lengths = lengths[:]  # assigned this way to make sure it's a deep copy and can freely be modified
        self._attenuation_coefficients = attenuation_coefficients[:]
        self._labels = labels[:]
        self._asymmetry_parameter = None
        self._check_validity()

    def _check_validity(self):
        if len(self._lengths) != len(self._attenuation_coefficients):
            raise ValueError("lengths and attenuation_coefficients need to be equally long lists.")
        if len(self._labels) != len(self._lengths) + 1:
            raise ValueError("labels needs to be a list with one more item than lengths and attenuation_coefficients.")

    def split_at_node(self, node_index):
        """Split fiber path into two fiber paths, the first ending at the node at which the second starts.

        Parameters
        ----------
        node_index : int
            Index of the node at which to split the fiber path. The indicated node will be the last node in the
            first fiber path and the first node in the second fiber path. Can take the value 1, 2, ...,
            :prop:`FiberPath.num_nodes`- 2, with 1 indicating a split at the first node after the first end node,
            and :prop:`FiberPath.num_Nodes` - 2  indicating a split at the last node before the second end node.

        Returns
        -------
        :class:`FiberPath`
            The first fiber path of the two created by the split. Ends at the node indicated by `node_index`.
        :class:`FiberPath`
            The second fiber path of the two created by the split. Starts at the node indicated by `node_index`.

        """
        self._check_if_valid_not_end_node(node_index)
        lengths_left_side = self._lengths[:node_index]
        att_coefs_left_side = self._attenuation_coefficients[:node_index]
        labels_left_side = self._labels[:node_index + 1]
        lengths_right_side = self._lengths[node_index:]
        att_coefs_right_side = self._attenuation_coefficients[node_index:]
        labels_right_side = self._labels[node_index:]
        left_fiber_path = FiberPath(lengths=lengths_left_side,
                                    attenuation_coefficients=att_coefs_left_side,
                                    labels=labels_left_side)
        right_fiber_path = FiberPath(lengths=lengths_right_side,
                                     attenuation_coefficients=att_coefs_right_side,
                                     labels=labels_right_side)
        return left_fiber_path, right_fiber_path

    def join(self, other_fiber_path):
        """Join two fiber paths to create a new fiber path. The first of the two fiber paths is the current object.

        The second fiber path is put at the end of the first fiber path. Therefore, the last node (second end node)
        of the first fiber path (the current object) should be the same node as the first node (first end node) of the
        second fiber path.

        Parameters
        ----------
        other_fiber_path : :class:`FiberPath`
            Fiber path to add at the end of the fiber path represented by the current object.

        Returns
        -------
        :class:`FiberPath`
            The fiber path obtained from joining the two fiber paths.

        Raises
        ------
        ValueError
            When the last node label (`FiberPath.labels()[-1]`) of the first fiber path is not equal to the
            first node label of the second fiber path (`FiberPath.labels()[0]`).

        """
        if self._labels[-1] != other_fiber_path.labels[0]:
            raise ValueError("Cannot join fiber paths because the label of the rightmost node of the first fiber path "
                             "is not the same as the label of the leftmost node of the second fiber path.")
        labels = self._labels[:-1]
        labels += other_fiber_path.labels
        lengths = self._lengths + other_fiber_path.lengths
        att_coefs = self._attenuation_coefficients + other_fiber_path.attenuation_coefficients
        return FiberPath(lengths=lengths, attenuation_coefficients=att_coefs, labels=labels)

    def remove_node(self, node_index):
        """Remove a node from the fiber path by directly connecting the fiber segments on both sides of the node.

        This operation reduces both the number of nodes and the number of fiber segments by one, but does not change
        the total length nor the total attenuation of the fiber path.

        Parameters
        ----------
        node_index : int
            Index of the node to remove from the fiber path.
            Can take the value 1, 2, ..., :prop:`FiberPath.num_nodes`- 2, with 1 indicating removal of the first node
            after the first end node, and :prop:`FiberPath.num_Nodes` - 2  indicating removal of the last node before
            the second end node.

        """
        self._check_if_valid_not_end_node(node_index)
        total_length_new_segment = self._lengths[node_index - 1] + self.lengths[node_index]
        total_loss_new_segment =\
            (self._attenuation_coefficients[node_index - 1] * self._lengths[node_index - 1] +
             self._attenuation_coefficients[node_index] * self._lengths[node_index])
        average_attenuation_new_segment = total_loss_new_segment / total_length_new_segment
        new_lengths = self._lengths[:node_index] + self._lengths[node_index + 1:]
        new_lengths[node_index - 1] = total_length_new_segment
        new_attenuation_coefficients = (self._attenuation_coefficients[:node_index] +
                                        self._attenuation_coefficients[node_index + 1:])
        new_attenuation_coefficients[node_index - 1] = average_attenuation_new_segment
        new_labels = self._labels[:node_index] + self._labels[node_index + 1:]
        return FiberPath(lengths=new_lengths, attenuation_coefficients=new_attenuation_coefficients, labels=new_labels)

    def _check_if_valid_not_end_node(self, node_index):
        """Check if the node index correctly points to one of the nodes between the end nodes."""
        if not isinstance(node_index, int):
            raise ValueError(f"node_index should be an int, not {type(node_index)}.")
        if not 1 <= node_index <= self.num_nodes - 2:
            raise ValueError(f"node_index should be the index of one of the nodes between the end nodes, "
                             f"not {node_index}.")

    @property
    def asymmetry_parameter(self):
        """The asymmetry parameter of the fiber path.

        The asymmetry parameter of a node (not end node) in the fiber path is given by
        the absolute value of difference between the distance to left neighbour and distance to right neighbour,
        divided by the sum of the two distances.
        The asymmetry parameter of the fiber path is defined as the average value over all node asymmetry parameters.

        """
        if self._asymmetry_parameter is None:
            node_asym_params = []
            for i in range(self.num_segments - 1):
                node_asym_param = (abs(self._lengths[i] - self._lengths[i + 1])
                                   / (self._lengths[i] + self._lengths[i + 1]))
                node_asym_params.append(node_asym_param)
            self._asymmetry_parameter = np.mean(node_asym_params)
        return self._asymmetry_parameter

    @property
    def lengths(self):
        """Lengths [km] of the fiber segments in the path."""
        return self._lengths

    @property
    def attenuation_coefficients(self):
        """Attenuation coefficients [db/km] of the fiber segments in the path."""
        return self._attenuation_coefficients

    @property
    def labels(self):
        """Labels of the nodes in the path."""
        return self._labels

    @property
    def num_segments(self):
        """Number of fiber segments in the path."""
        return len(self._lengths)

    @property
    def num_nodes(self):
        """Number of nodes in the path, including the end nodes."""
        return len(self._labels)

    @property
    def total_length(self):
        return sum(self._lengths)
