import os
import warnings
import yaml
import pickle
from netsquid_netconf.netconf import Loader
from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder
from netsquid_qrepchain.simulations.unified_simulation_script_state import save_data, add_mock_data, \
    run_unified_simulation_state
from generate_configurations.generate_config_files import create_config_file_from_lookup, \
    create_lookup_table_ordered_on_asymmetry
from optimization_machinery.analytical_model_for_one_repeater_key_rates import analyze_bb84_performance
from argparse import ArgumentParser
from ruamel.yaml import YAML


def read_chain_parameters_from_param_file(param_file):
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)
    return sim_params["n_repeaters"], sim_params["asym_degree"]


def determine_asym_id_from_degree(table, n_repeaters, asym_degree):
    return int((len(table[n_repeaters]) - 1) * asym_degree)


def config_file_from_fiber_data_and_chain_parameters(fiber_data_file, config_file_name, param_file):
    """Generates configuration file from fiber data and chain parameters.

    Note: The tools in `simulations.deutsche_telekom.generate_config_files` make reference to an `asym_id` parameter.
    A more detailed description can be found there, but this essentially corresponds to an index that ranks a given
    repeater chain configuration among repeater chains with the same number of repeaters based on its degree of
    asymmetry. Given that the number of valid repeater chain configurations depends on the number of repeaters being
    placed, the range over which this parameter should be optimized depends also on the number of repeaters, which can
    be another optimization parameter. To get around this, we optimize instead over what we call the `asym_degree`. This
    is a real number between 0 and 1 that is meant to represent the "relative degree of asymmetry" of a repeater chain
    configuration w.r.t. other chains with the same number of repeaters. This means that an `asym_degree` of 0.5 should
    correspond to the repeater chain with an "average" `asym_id`. For example if there are 10 repeater chain
    configurations (`asym_id` ranging from 0 to 9), an `asym_id` of 0.5 would correspond to the chain with `asym_id` 4.
    The conversion between these two parameters is handled by the `determine_asym_id_from_degree` function.

    Parameters
    ----------
    fiber_data_file : str
        Name of file holding fiber data.
    config_file_name : str
        Name of configuration file to be generated.
    param_file : str
        Name of parameter file that should be included in the generated configuration file.

    """
    n_repeaters, asym_degree = read_chain_parameters_from_param_file(param_file=param_file)
    config_file_filename = config_file_name
    lookup_table_filename = "rep_chain_lookup_table.pickle"
    if not os.path.isfile(lookup_table_filename):
        lookup_table = create_lookup_table_ordered_on_asymmetry(fiber_data_filename=fiber_data_file,
                                                                lookup_table_filename=lookup_table_filename)
    else:
        while True:
            # when running on a cluster, it might be that multiple threads try to access the file simultaneously,
            # leading to an error being thrown. this implements a brute-force solution in which we just keep trying
            # until we can access the file
            try:
                lookup_table = pickle.load(open(lookup_table_filename, "rb"))
                break
            except EOFError:
                pass
    asym_id = determine_asym_id_from_degree(table=lookup_table,
                                            n_repeaters=n_repeaters,
                                            asym_degree=asym_degree)
    create_config_file_from_lookup(n_reps=n_repeaters,
                                   identifier=asym_id,
                                   lookup_table_filename=lookup_table_filename,
                                   config_file_filename=config_file_filename,
                                   node_type="depolarizing_node",
                                   params_filename=param_file)


def extract_overall_attenuation_and_length_per_link(config_file):
    """Reads configuration file and extracts overall attenuation and length for each elementary link.

    Parameters
    ----------
    config_file : str
        Name of configuration file.

    Returns
    -------
        list, list

    """
    yaml = YAML()
    with open(config_file) as file:
        config = yaml.load(file)
    lengths = []
    attenuations = []
    for key, val in config["components"].items():
        if "ent_connection" in key:
            length = val["properties"]["length"]
            lengths.append(length)
            attenuations.append(val["properties"]["attenuation_coefficient"] * length)

    return attenuations, lengths


def compute_mode_success_probability_per_link(attenuations, efficiency):
    """

    Parameters
    ----------
    attenuations: list of floats
        Each entry is the overall attenuation, in dB, of an elementary link.
    efficiency : float
        Probability of photon survival, excluding fiber attenuation.

    Returns
    -------
        list

    """
    mode_success_probabilities = []
    for att in attenuations:
        mode_success_probabilities.append(10 ** (- att / 10) * efficiency)
    return mode_success_probabilities


def compute_overall_success_probability_per_link(mode_success_probabilities, n_modes):
    """

    Parameters
    ----------
    mode_success_probabilities : list of floats
        Each entry is the success probability of a single mode in an elementary link.
    n_modes : int
        Number of multiplexing modes.

    Returns
    -------
        list

    """
    overall_success_probabilities = []
    for p_suc in mode_success_probabilities:
        overall_success_probabilities.append(1 - (1 - p_suc) ** n_modes)
    return overall_success_probabilities


def compute_round_times(lengths, speed_of_light):
    """

    Parameters
    ----------
    lengths : list of floats
        Each entry is the length, in km, of an elementary link.
    speed_of_light : float [km/s]
        Speed of light in fiber.

    Returns
    -------
        float

    """
    round_times = []

    for length in lengths:
        round_times.append(length / speed_of_light)

    return round_times


def attenuations_and_lengths_of_all_links_of_single_repeater_segments(attenuations, lengths):
    """Breaks up chain defined by given attenuations and lengths into all of its possible single-repeater segments by
    glueing together consecutive elementary links. Symmetrizes the single-repeater segment by making the longer,
    more-lossy side as short and lossy as the other side, resulting in an upper-bound on the expected performance.

    Parameters
    ----------
    attenuations: list of floats
        Each entry is the overall attenuation, in dB, of an elementary link.
    lengths : list of floats
        Each entry is the length, in km, of an elementary link.

    Returns
    -------
        list, list

    """

    single_repeater_segment_attenuations = []
    single_repeater_segment_lengths = []

    for i, (attenuation, length) in enumerate(zip(attenuations, lengths)):
        single_repeater_segment_attenuations.append(min(attenuation, attenuations[i + 1]))
        single_repeater_segment_lengths.append(min(length, lengths[i + 1]))
        if i == len(attenuations) - 2:
            break

    return single_repeater_segment_attenuations, single_repeater_segment_lengths


def simulation_should_be_run(config_file, param_file, target_rate):
    """Determines whether or not simulation should be run.

    To do this, we compute an upper bound on the expected entanglement generation rate for each single-repeater segment
    in the chain. If any of these is smaller than the target BQC success rate, then we can be sure that the
    end-to-end BQC success rate would also be smaller than the target BQC success rate and running the
    simulation would be pointless.

    We use the `analyze_bb84_performance` function, which computes the expected entanglement generation rate of a
    symmetric single sequential repeater. We multiply the result obtained by two, as we are interested in repeaters with
    two-sided entanglement generation, whose rate is upper bounded by twice the rate of a single sequential repeater.

    Parameters
    ----------
    config_file : str
        Name of configuration file.
    param_file : str
        Name of parameter file.
    target_rate : int [Hz]
        Target BQC success rate.

    Returns
    -------
        bool

    """
    attenuations, lengths = extract_overall_attenuation_and_length_per_link(config_file=config_file)
    attenuations, lenghts = attenuations_and_lengths_of_all_links_of_single_repeater_segments(attenuations=attenuations,
                                                                                              lengths=lengths)
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)
    round_times = compute_round_times(lengths=lengths, speed_of_light=sim_params["speed_of_light"])
    mode_success_probabilities = compute_mode_success_probability_per_link(attenuations=attenuations,
                                                                           efficiency=sim_params['efficiency'])
    overall_success_probabilities = compute_overall_success_probability_per_link(
        mode_success_probabilities=mode_success_probabilities,
        n_modes=sim_params['num_modes'])
    rate_upper_bounds = []
    coherence_time = sim_params["coherence_time"] if sim_params["coherence_time"] != 0 else 1.e+100
    for success_probability, round_time in zip(overall_success_probabilities, round_times):
        _, _, distribution_time = analyze_bb84_performance(link_success_prob=success_probability,
                                                           round_time=round_time,
                                                           coherence_time=coherence_time,
                                                           cutoff_time=sim_params["cutoff_time"],
                                                           only_dephasing=False,
                                                           measure_directly=False)
        rate_upper_bounds.append(2 / distribution_time)
    return min(rate_upper_bounds) > target_rate


def _cutoff_coherence_time_heuristic(sim_params, target_rate):
    if sim_params["coherence_time"] > 10. / target_rate:
        warnings.warn("Removing cutoff time because coherence time is long enough to make it irrelevant.")
        sim_params["cutoff_time"] = None


def _translate_smart_stopos_blueprint(param_file, sim_params):
    if "cutoff_time" in sim_params and sim_params["cutoff_time"] is not None:
        sim_params["cutoff_time"] = sim_params["cutoff_time"] * sim_params["coherence_time"]

    with open(param_file, "w") as stream:
        yaml.dump(sim_params, stream)


def read_sim_params(param_file):
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream=stream, Loader=Loader)

    return sim_params


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('configfile', type=str, help="Name of the config file.")
    parser.add_argument('--fiber_data_file', type=str, help="Name of the file with fiber data.")
    parser.add_argument('--target_rate', required=True, type=float,
                        help="Target BQC success rate we are optimizing for, in Hz.")
    parser.add_argument('-pf', '--paramfile', required=False, type=str,
                        help="Name of the parameter file.")
    parser.add_argument('-n', '--n_runs', required=False, type=int, default=10,
                        help="Number of runs per configuration. If none is provided, defaults to 10.")
    parser.add_argument('--output_path', required=False, type=str, default="raw_data",
                        help="Path relative to local directory where simulation results should be saved.")
    parser.add_argument('--filebasename', required=False, type=str, help="Name of the file to store results in.")
    parser.add_argument('--plot', dest="plot", action="store_true", help="Plot the simulation results.")

    args, unknown = parser.parse_known_args()

    sim_params = read_sim_params(args.paramfile)
    _translate_smart_stopos_blueprint(args.paramfile, sim_params)
    _cutoff_coherence_time_heuristic(sim_params, args.target_rate)
    try:
        if not simulation_should_be_run(config_file=args.configfile,
                                        param_file=args.paramfile,
                                        target_rate=args.target_rate):
            warnings.warn("Using mock data because parameters are such that target rate is not achievable.")
            with open(args.paramfile, "r") as stream:
                sim_params = yaml.load(stream=stream, Loader=Loader)
            dataframe = add_mock_data(dataframe=None,
                                      n_runs=args.n_runs * 2,
                                      duration=1e15)
            repchain_df_holder = RepchainDataFrameHolder(baseline_parameters=sim_params,
                                                         data=dataframe,
                                                         number_of_nodes=2)
        else:
            repchain_df_holder, _ = run_unified_simulation_state(configfile=args.configfile,
                                                                 paramfile=args.paramfile,
                                                                 n_runs=args.n_runs,
                                                                 suppress_output=False,
                                                                 max_run_duration=1.e+15,
                                                                 use_mock_data=True)
    except ZeroDivisionError:
        warnings.warn("Using mock data because elementary link success probability was too low.")
        with open(args.paramfile, "r") as stream:
            sim_params = yaml.load(stream=stream, Loader=Loader)
        dataframe = add_mock_data(dataframe=None,
                                  n_runs=args.n_runs * 2,
                                  duration=1e15)
        repchain_df_holder = RepchainDataFrameHolder(baseline_parameters=sim_params,
                                                     data=dataframe,
                                                     number_of_nodes=2)
    save_data(repchain_df_holder, args)
