# Simulation code for Requirements for upgrading trusted-nodes to a repeater chain over 900 km of optical fiber

This repository contains all the code required to reproduce the simulation data presented in the paper
``Requirements for upgrading trusted-nodes to a repeater chain over 900 km of optical fiber'',
by Francisco Ferreira da Silva, Guus Avis and Stephanie Wehner.

The data generated with this code that was made to create the figures in the paper can be found in [another repository]().


## Getting Started

To run the code in this repository, you need to have the required python packages installed.
These required packages are listed in `requirements.txt` and can be automatically installed by typing `make requirements` in the terminal.
Note though that one of the requirements is the NetSquid package, and in order to install this you need to use your NetSquid credentials (these are the same as for the [NetSquid forum](https://forum.netsquid.org/)).
To use the `make install` command to install netsquid, you need to first set your NetSquid credentials as environment variables.
This can be done using the following commands:

`export NETSQUIDPYPI_USER=<netsquid-forum-username>`

`export NETSQUIDPYPI_PWD=<netsquid-forum-password>`

After you have done this, `make requirements` should take care of everything.
To check whether you have installed all packages successfully and you are able to run the code in this repository, you can do `make tests` to run a series of unittests.


## Folders

This repository is divided into a number of folders.
They are as follows:

* `minimal_number_modes`. This folder contains the code required to determine what the minimal number of multiplexing modes is to reach a specific target secret-key rate (`determine_minimal_number_modes.py`). It contains eight configuration files on which the script can be run. These correspond to a symmetrized version of the Deutsche Telekom fiber path with different number of repeaters.

* `optimization_machinery`. This folder contains tools required to perform the genetic-algorithm optimization used in the paper to find the minimal hardware requirements given a fiber path, target performance metric and baseline hardware parameters.

* `generate_configurations`. This folder contains the code needed to generate configurations files corresponding to all the possible ways repeaters can be placed in the Deutsche Telekom fiber path.

* `perform_optimizations`: This folder contains the code, scripts and configuration files necessary to perform the optimizations for all the scenarios we investigate in the paper. For more details, see the folder's README.
